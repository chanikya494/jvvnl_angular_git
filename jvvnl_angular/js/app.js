﻿(function () {
    'use strict';

    angular.module('app', ['ngRoute'])
        .config(config)
    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];

    function config($routeProvider, $locationProvider, $httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        debugger;
        $routeProvider
            .when('/', {
                controller: 'loginController',
                templateUrl: 'templates/login.html',
            })

            .when('/home', {
                controller: 'homeController',
                templateUrl: 'templates/home.html',
            })



            .otherwise({ redirectTo: '/' });
    }
})();