﻿function StatusCheck_valid(sid, sframe,smeter) {
    if (sid == "feder") {
        document.getElementById("frmConnection").src = 'frmFeederMasterData.aspx?meter=' + smeter;
    }
    if (sid == "instant") {
        document.getElementById("frmInstant").src = 'frmInstants.aspx?meter=' + smeter;
    }
    if (sid == "event") {
        document.getElementById("frmEvent").src = 'frmMeterEvents.aspx?meter=' + smeter;
    }
    if (sid == "block") {
        document.getElementById("frmBlock").src = 'frmBlockLoad.aspx?meter=' + smeter;
    }
    if (sid == "daily") {
        document.getElementById("frmDaily").src = 'frmDailyLoad.aspx?meter=' + smeter;
    }
    if (sid == "voltage") {
        document.getElementById("frmVoltage").src = 'frmVoltageGraph.aspx?meter=' + smeter;
    }
    if (sid == "current") {
        document.getElementById("frmCurrent").src = 'frmCurrentGraph.aspx?meter=' + smeter;
    }
    if (sid == "avgpf") {
        document.getElementById("frmAvgPF").src = 'frmPowerFactorGraph.aspx?meter=' + smeter;
    }
    if (sid == "energy") {
        document.getElementById("frmEnergy").src = 'frmEnergyGraph.aspx?meter=' + smeter;
    }
    return true;
}