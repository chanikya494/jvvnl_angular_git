﻿var app = angular.module('myapp', ['ngMaterial', 'ngCookies', 'ngMessages', 'material.svgAssetsCache', 'ui.grid', 'ui.directives', 'ui.grid.expandable', 'ui.grid.pagination', 'ui.grid.exporter', 'ngToast']);
var meterno, userid, zone, circle, mla, division, subdivision, jen, pdate, feederName, feederType, Area, subStation, image_pdf;

app.config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});

app.service('feederMasterService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getFeederDetails/";
    this.feederMaster = function (meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno).then(function (feederdata) {
                resolve(feederdata);
            })
        })
    }
});

app.service('analysisService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getFeederSummary/";
    this.analysis = function (meterno, sdate, edate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno + "/" + sdate + "/" + edate).then(function (analysisdata) {
                resolve(analysisdata);
            })
        })
    }
})

app.service('meterEventsService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getMeterEvents/";
    this.meterevents = function (meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno).then(function (metereventsdata) {
                resolve(metereventsdata);
            })
        })
    }
})

app.service('loadSurveyService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getLoadSurvey/";
    this.loadsurvey = function (meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno).then(function (loadSurveyData) {
                resolve(loadSurveyData);
            })
        })
    }
})

app.service('dailyLoadService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getDailyLoad/";
    this.dailyLoad = function (meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno).then(function (dailyLoadData) {
                resolve(dailyLoadData);
            })
        })
    }
})

app.service('logBookService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/";
    this.logEnergy = function (userid, zoe, circle, mla, division, subdivision, jen, logdate, meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "getChild/" + userid + "/" + zoe + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + logdate + "/" + meterno).then(function (logchildData) {
                resolve(logchildData);
            })
        })
    };

    this.merge = function (meterno, date) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "getMergeData/" + meterno + "/" + date).then(function (mergeData) {
                resolve(mergeData);
            })
        })
    };

    this.logSupply = function (metero, date) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "getSupplyLog/" + meterno + "/" + date).then(function (supplyData) {
                resolve(supplyData);
            })
        })
    }

});

app.service('voltageGraphService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getVoltageGraph/";
    this.voltageGraph = function (meterno, status) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno + "/" + status).then(function (voltageData) {
                resolve(voltageData);
            })
        })
    }
});

app.service('instantsService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getInstants/";
    this.instants = function (meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno).then(function (instantsData) {
                resolve(instantsData);
            })
        })
    }
})

app.controller('feederMasterController', function ($scope, $window, $cookies, $http, feederMasterService, $location, $cookies) {
    meterno = $location.search().meterNo;
    zone = $location.search().zone;
    circle = $location.search().circle;
    mla = $location.search().mla;
    division = $location.search().div;
    subdivision = $location.search().subDiv;
    jen = $location.search().jen;
    pdate = $location.search().date;
    $('#danceforme').show();
    $scope.Name = $cookies.get('myFavorite');
    $scope.lablel = $window.Test;
    $scope.feedername = $window.feedername;
    $scope.type = $window.type;
    $scope.area = $window.area;
    $scope.substation = $window.substation;
    $scope.meterno = meterno;
    userid = $cookies.get('userid');

    function toDataURL(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    };
    var pth = window.location.href;/*get path of image folder*/
    var full_path = pth.replace(pth.substr(pth.lastIndexOf('/') + 1), '');

    toDataURL(full_path + '/images/vlogo.png', function (dataUrl) {
        image_pdf = dataUrl;
        console.log(image_pdf);
    });

    $scope.openMap = function () {
        var x = screen.width / 2 - 1250 / 2;
        var y = screen.height / 2 - 740 / 2;
        $window.open("https://fdrms.visiontek.co.in:9004/SubstationMap/FeederMap.aspx?userid=" + userid + "&zone=" + $scope.zone.replace('&', '$') + "&circle=" + $scope.circle.replace('&', '$') + "&mla=" + $scope.mla.replace('&', '$') + "&div=" + $scope.division.replace('&', '$') + "&sdiv=" + $scope.subdivision + "&jen=" + $scope.jen + "&substation=" + $scope.substation + "", "_blank", "height=710,width=1250,status=yes,left=" + x + ",top=" + y);
    }

    feederMasterService.feederMaster($scope.meterno).then(function (data) {
        $scope.MF = data.data[0].MF;
        $scope.YOM = data.data[0].YOM;
        $scope.aen_name = data.data[0].aen_name;
        $scope.aen_ph = data.data[0].aen_ph;
        $scope.area = Area = data.data[0].area;
        $scope.ce_name = data.data[0].ce_name;
        $scope.ce_ph = data.data[0].ce_ph;
        $scope.circle = data.data[0].circle;
        $scope.ctr_inter = data.data[0].ctr_inter;
        $scope.division = data.data[0].division;
        $scope.feederName = feederName = data.data[0].feederName;
        $scope.feederNo = data.data[0].feederNo;
        $scope.feederType = feederType = data.data[0].feederType;
        $scope.fi_name = data.data[0].fi_name;
        $scope.fi_ph = data.data[0].fi_ph;
        $scope.jen = data.data[0].jen;
        $scope.jen_name = data.data[0].jen_name;
        $scope.jen_ph = data.data[0].jen_ph;
        $scope.latitude = data.data[0].latitude;
        $scope.longitude = data.data[0].longitude;
        $scope.meterFirm = data.data[0].meterFirm;
        $scope.meterno = data.data[0].meterno;
        $scope.metertype = data.data[0].metertype;
        $scope.mla = data.data[0].mla;
        $scope.mla_name = data.data[0].mla_name;
        $scope.mla_ph = data.data[0].mla_ph;
        $scope.modem_ph = data.data[0].modem_ph;
        $scope.modem_sno = data.data[0].modem_sno;
        $scope.ptr_inter = data.data[0].ptr_inter;
        $scope.se_name = data.data[0].se_name;
        $scope.se_ph = data.data[0].se_ph;
        $scope.subdivision = subdivision = data.data[0].subdivision;
        $scope.substation = subStation = data.data[0].substation;
        $scope.substation_ph = data.data[0].substation_ph;
        $scope.village = data.data[0].village;
        $scope.xen_name = data.data[0].xen_name;
        $scope.xen_ph = data.data[0].xen_ph;
        $scope.zone = data.data[0].zone;
    });


});

app.controller('analysisControler', function ($scope, $filter, analysisService) {


    $scope.model3 = {
        startDate: pdate,
        endDate: pdate,
        toDay: $filter('date')(new Date(), 'dd/MM/yyyy')
    };
    //alert($scope.model3.startDate);
    var sdate = $filter('date')($scope.model3.startDate, 'yyyy-MM-dd');
    var edate = $filter('date')($scope.model3.startDate, 'yyyy-MM-dd');

    $scope.goFunction = function () {
        sdate = dateFormatter($scope.model3.startDate);
        edate = dateFormatter($scope.model3.endDate);
        function dateFormatter(sdate) {
            var fields = sdate.split('/');
            var date1 = fields[2] + '-' + fields[1] + '-' + fields[0];
            return date1;
        };
        callService(meterno, sdate, edate);
    };
    callService(meterno, sdate, edate);

    function callService(meterno, sdate, edate) {
        sdate = $filter('date')(new Date(sdate), 'yyyy-MM-dd');
        edate = $filter('date')(new Date(edate), 'yyyy-MM-dd');
        analysisService.analysis(meterno, sdate, edate).then(function (data) {
            $scope.AG_Cons = data.data[0].AG_Cons;
            $scope.AvgCur = data.data[0].AvgCur;
            $scope.AvgMDkVA = data.data[0].AvgMDkVA;
            $scope.AvgMdKW = data.data[0].AvgMdKW;
            $scope.AvgVolt = data.data[0].AvgVolt;
            $scope.DL_Cons = data.data[0].DL_Cons;
            $scope.HighDur = data.data[0].HighDur;
            $scope.MaxCur = data.data[0].MaxCur;
            $scope.MaxMDkVA = data.data[0].MaxMDkVA;
            $scope.MaxVolt = data.data[0].MaxVolt;
            $scope.MinCur = data.data[0].MinCur;
            $scope.MinMDKw = data.data[0].MinMDKw;
            $scope.MinVolt = data.data[0].MinVolt;
            $scope.Single_ph = data.data[0].Single_ph;
            $scope.Three_ph = data.data[0].Three_ph;
            $scope.TotalInt = data.data[0].TotalInt;
            $scope.TotalIntDur = data.data[0].TotalIntDur;

            if (data.data[0].AG_Cons === '' && data.data[0].DL_Cons === '') {
                $scope.AG_DG_Total = '';
            }

            else {
                if (data.data[0].AG_Cons != '' && data.data[0].DL_Cons === '') {
                    $scope.AG_DG_Total = data.data[0].AG_Cons;
                }

                if (data.data[0].AG_Cons === '' && data.data[0].DL_Cons != '') {
                    $scope.AG_DG_Total = data.data[0].DL_Cons;
                }

                if (data.data[0].AG_Cons != '' && data.data[0].DL_Cons != '') {
                    $scope.AG_DG_Total = data.data[0].kWh;
                }
            }

            $scope.avgPF = data.data[0].avgPF;
            $scope.kVAh = data.data[0].kVAh;
            $scope.kVArh_lag = data.data[0].kVArh_lag;
            $scope.kVArh_lead = data.data[0].kVArh_lead;
            $scope.kWh = data.data[0].kWh;
            $scope.lowDur = data.data[0].lowDur;
            $scope.maxMDKw = data.data[0].maxMDKw;
            $scope.maxPF = data.data[0].maxPF;
            $scope.minMDkVA = data.data[0].minMDkVA;
            $scope.minPF = data.data[0].minPF;
        });
    };

});

app.controller('meterEventsController', function ($scope, $filter, meterEventsService) {
    $scope.eventsgridOptions = {
        enableGridMenu: true,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: true,
        enableFiltering: true,
        columnDefs: [
            { field: 'Sno', displayName: 'S. No', enableColumnMenu: false },
            { field: 'meterno', displayName: 'Meter No', enableColumnMenu: false },
            { field: 'edate', displayName: 'Meter Date', enableColumnMenu: false, width: "*" },
            { field: 'status', displayName: 'Status', enableColumnMenu: false },
            { field: 'vr', displayName: 'VR(kV)', enableColumnMenu: false },
            { field: 'vy', displayName: 'VY(kV)', enableColumnMenu: false },
            { field: 'vb', displayName: 'VB(kV)', enableColumnMenu: false },
            { field: 'vry', displayName: 'VRY(kV)', enableColumnMenu: false },
            { field: 'vyb', displayName: 'VYB(kV)', enableColumnMenu: false },
            { field: 'ir', displayName: 'IR', enableColumnMenu: false },
            { field: 'iy', displayName: 'IY', enableColumnMenu: false },
            { field: 'ib', displayName: 'IB', enableColumnMenu: false },
            { field: 'qr', displayName: 'QR', enableColumnMenu: false },
            { field: 'qy', displayName: 'QY', enableColumnMenu: false },
            { field: 'qb', displayName: 'QB', enableColumnMenu: false },
            { field: 'cum_kWh', displayName: 'Cum. kWh', enableColumnMenu: false },
        ]
    };

    meterEventsService.meterevents(meterno).then(function (data) {
        $scope.eventsgridOptions.data = data.data;
    });

});

app.controller('blockLoadController', function ($scope, loadSurveyService) {
    $scope.blockLoadgridOptions = {
        enableGridMenu: true,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: true,
        enableFiltering: true,
        columnDefs: [
            { field: 'Sno', displayName: 'S. No', enableColumnMenu: false },
            { field: 'Meterno', displayName: 'Meter No', enableColumnMenu: false },
            { field: 'mdate', displayName: 'Meter Date', enableColumnMenu: false, width: "*" },
            { field: 'vr', displayName: 'VR(kV)', enableColumnMenu: false },
            { field: 'vy', displayName: 'VY(kV)', enableColumnMenu: false },
            { field: 'vb', displayName: 'VB(kV)', enableColumnMenu: false },
            { field: 'ir', displayName: 'IR', enableColumnMenu: false },
            { field: 'iy', displayName: 'IY', enableColumnMenu: false },
            { field: 'ib', displayName: 'IB', enableColumnMenu: false },
            { field: 'kwh', displayName: 'kWh', enableColumnMenu: false },
            { field: 'kvarh_lg', displayName: 'kVArh Lag', enableColumnMenu: false },
            { field: 'kvarh_ld', displayName: 'kVArh Lead', enableColumnMenu: false },
            { field: 'kvarh', displayName: 'kVAh', enableColumnMenu: false },
            { field: 'freq', displayName: 'Freq.', enableColumnMenu: false },
        ]
    };

    loadSurveyService.loadsurvey(meterno).then(function (data) {
        $scope.blockLoadgridOptions.data = data.data;
    });
});

app.controller('dailyLoadController', function ($scope, dailyLoadService) {
    $scope.dailyLoadGridOptions = {
        enableGridMenu: true,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: true,
        enableFiltering: true,
        columnDefs: [
            { field: 'Sno', displayName: 'S. No', enableColumnMenu: false },
            { field: 'Meterno', displayName: 'Meter No', enableColumnMenu: false },
            { field: 'mdate', displayName: 'Meter Date', enableColumnMenu: false, width: "*" },
            { field: 'cumkwh_imp', displayName: 'Cum.kWh Import', enableColumnMenu: false },
            { field: 'cumkwh_exp', displayName: 'Cum.kWh Export', enableColumnMenu: false },
            { field: 'cumkvah_kwimp', displayName: 'Cum.kVAh Import', enableColumnMenu: false },
            { field: 'cumkvah_kwexp', displayName: 'Cum.kVAh Export', enableColumnMenu: false },
            { field: 'cumkvarh_q1', displayName: 'Cum.kVArh q1', enableColumnMenu: false },
            { field: 'cumkvarh_q2', displayName: 'Cum.kVArh q2', enableColumnMenu: false },
            { field: 'cumkvarh_q3', displayName: 'Cum.kVArh q3', enableColumnMenu: false },
            { field: 'cumkvarh_q4', displayName: 'Cum.kVArh q4', enableColumnMenu: false },
            { field: 'meterDate', enableColumnMenu: false }
        ]
    };

    dailyLoadService.dailyLoad(meterno).then(function (data) {
        $scope.dailyLoadGridOptions.data = data.data;
    });
});

app.controller('logEnergyController', function ($scope, logBookService, $filter) {
    var yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
    $("#txtfromDate").datepicker({
        showOn: 'button',
        buttonText: "day",
        buttonImage: 'images/cal.gif',
        buttonImageOnly: false,
        dateFormat: 'dd-mm-yy',
        maxDate: yesterday
    });

    $scope.txtDate = $filter('date')(pdate, 'dd-MM-yyyy');

    $scope.goClick = function (date) {
        date = $filter('date')(new Date(date), 'dd-MM-yyyy');
        logBookService.logEnergy(userid, zone, circle, mla, division.replace('&', '$'), subdivision.replace('&', '$'), jen, date, meterno).then(function (data) {
            $scope.logChildGridOptions.data = data.data;
        });

        logBookService.merge(meterno, date).then(function (data) {
            $scope.threePhaseCons = data.data[0].tr_ph_con;
            $scope.twoPhaseCons = data.data[0].tw_ph_con;
            $scope.singlePhaseCons = data.data[0].si_ph_con;
        })
    }

    $scope.logChildGridOptions = {
        enableGridMenu: true,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: true,
        enableFiltering: true,
        columnDefs: [
            { field: 'SNo', displayName: 'S. No', enableColumnMenu: false },
            { field: 'DateTime', displayName: 'Date', enableColumnMenu: false },
            { field: 'MF', displayName: 'MF', enableColumnMenu: false },
            { field: 'Reading', displayName: 'Reading', enableColumnMenu: false },
            { field: 'load_cur', displayName: 'Current', enableColumnMenu: false },
            { field: 'load_kva', displayName: 'Voltage', enableColumnMenu: false },
            { field: 'kwh_Cons', displayName: 'kWh Cons.', enableColumnMenu: false },
            { field: 'kvarh_lead', displayName: 'kVarh Lead', enableColumnMenu: false },
            { field: 'kvarh_lag', displayName: 'kVarh Lag', enableColumnMenu: false },
            { field: 'Ir', displayName: 'Ir', enableColumnMenu: false },
            { field: 'Iy', displayName: 'Iy', enableColumnMenu: false },
            { field: 'Ib', displayName: 'Ib', enableColumnMenu: false },
            { field: 'Vr', displayName: 'Vr', enableColumnMenu: false },
            { field: 'Vy', displayName: 'Vy', enableColumnMenu: false },
            { field: 'Vb', displayName: 'Vb', enableColumnMenu: false },
            { field: 'PF', displayName: 'PF', enableColumnMenu: false },
            { field: 'EventDesc', displayName: 'Event Desc.', enableColumnMenu: false }
        ]
    };

    logBookService.logEnergy(userid, zone, circle, mla, division.replace('&', '$'), subdivision.replace('&', '$'), jen, pdate, meterno).then(function (data) {
        $scope.logChildGridOptions.data = data.data;
    });

    logBookService.merge(meterno, pdate).then(function (data) {
        $scope.threePhaseCons = data.data[0].tr_ph_con;
        $scope.twoPhaseCons = data.data[0].tw_ph_con;
        $scope.singlePhaseCons = data.data[0].si_ph_con;
    })
});

app.controller('logSupplyController', function ($scope, logBookService, $filter, $http) {

    $scope.logSupplyPDF = function () {
        debugger;
        $http.get('http://localhost:55130/Service1.svc/getSupplyLog/' + meterno + "/" + date).then(function (response) {
            debugger;
            var data = [];

            var temp1 = [

                    { text: '', style: 'tableHeader' },
                    { text: '3-Ph Supply ', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: '2-Ph Supply', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: '1-Ph Supply', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: 'Outage', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: '', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}

            ];

            var temp2 = [

                    { text: 'Date', style: 'tableHeader', alignment: 'center' },
                    { text: 'Hrs.', style: 'tableHeader', alignment: 'center' },
                    { text: 'Timings and Duration', style: 'tableHeader', alignment: 'center' },
                    { text: 'Hrs.', style: 'tableHeader', alignment: 'center' },
                    { text: 'Timings and Duration', style: 'tableHeader', alignment: 'center' },
                    { text: 'Hrs.', style: 'tableHeader', alignment: 'center' },
                    { text: 'Timings and Duration', style: 'tableHeader', alignment: 'center' },
                    { text: 'Hrs.', style: 'tableHeader', alignment: 'center' },
                    { text: 'Timings and Duration', style: 'tableHeader', alignment: 'center' },
                    { text: 'Total Supply(Hrs.)', style: 'tableHeader', alignment: 'center' },
                    { text: 'Remarks', style: 'tableHeader', alignment: 'center' }

            ];

            for (var i = 0; i < response.data.length; i++) {
                data[i] = new Array(11);
                data[i][0] = (response.data[i].date);
                data[i][1] = response.data[i].threePhaseDuration;
                data[i][2] = (response.data[i].threePhaseTimeDuration);
                data[i][3] = response.data[i].twoPhaseDuration;
                data[i][4] = (response.data[i].twoPhaseTimeDuraton);
                data[i][5] = response.data[i].singlePhaseDuration;
                data[i][6] = (response.data[i].singlePhaseTimeDuration);
                data[i][7] = response.data[i].ourtageDuration;
                data[i][8] = (response.data[i].outageTimeDuration);
                data[i][9] = response.data[i].totalSupplyDuration;
                data[i][10] = response.data[i].remarks;
            }
            debugger;
            data.splice(0, 0, temp1);
            data.splice(1, 0, temp2);
            console.log(data);

            var date1 = new Date(date), locale = "en-us", month = date1.toLocaleString(locale, { month: "long" });
            var curDate = new Date().toLocaleString();
            var dd = {
                pageOrientation: 'landscape',
                pageMargins: [40, 60, 40, 30],
                header: {
                    margin: 10,
                    columns: [
                        {
                            margin: [0, 20, 10, 0],
                            alignment: 'right',
                            fit: [100, 100],
                            image: 'building',

                        }
                    ]
                },
                content: [
                    { text: 'Feeder Supply Details for the month ' + month, alignment: 'center', bold: true, fontSize: 11, margin: [0, 0, 0, 0] },
                    { text: 'Date: ' + curDate, alignment: 'right', fontSize: 8, margin: [0, 10, 0, 10] },
                    {
                        style: 'tableExample',
                        color: '#444',
                        table: {
                            widths: [23, 'auto', 110, 'auto', 110, 'auto', 110, 'auto', 110, 'auto', 'auto'],
                            //widths: ['auto', 'auto', 'auto'],
                            headerRows: 2,
                            // keepWithHeaderRows: 1,
                            body: data
                        }
                    },

                ],
                styles: {
                    header: {
                        fontSize: 8,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                    subheader: {
                        fontSize: 6,
                        bold: true,
                        margin: [0, 10, 0, 5]
                    },
                    tableExample: {
                        margin: [0, 5, 0, 15],
                        fontSize: 9,
                    },
                    tableHeader: {
                        bold: true,
                        fontSize: 9,
                        color: 'black',
                        fillColor: '#ee9800',
                    }
                },
                images: {
                    building: image_pdf
                },
                defaultStyle: {
                    // alignment: 'justify'
                }

            }
            pdfMake.createPdf(dd).download("logSupply_" + date + ".pdf");
        });
    };

    $('#txtLogSupply').focusin(function () {
        $('.ui-datepicker-calendar').css("display", "none");
    });

    $scope.txtLogSupplyDate = new Date($scope.txtLogSupplyDate);

    var date = $filter('date')(new Date(pdate), 'MMMM yyyy');
    $scope.txtLogSupplyDate = date;

    $scope.supplyDateOpts = {
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,
        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            seriesCounter = 0;
            date = $filter('date')(new Date(iYear, iMonth), 'MMMM yyyy');
            //getLogSupplyTable($filter('date')(new Date(iYear, iMonth), 'MMMM yyyy'));
        },
        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            }
        }
    };

    getLogSupplyTable(date);

    $scope.logSupplyClick = function () {
        getLogSupplyTable(date);
    }

    function getLogSupplyTable(date) {
        $("#tblLogSupply").DataTable({
            "bDestroy": true,
            "processing": true,
            "paging": false,
            "bAutoWidth": false,
            dom: 'lBfrtip',
            // Column definitions
            "ordering": false,
            "order": [],
            "columnDefs": [{
                "targets": '0',
                "orderable": false,
            }],
            "ajax": {
                "url": "http://localhost:55130/Service1.svc/getSupplyLog/" + meterno + "/" + date,
                "dataSrc": ""
            },
            "columns": [
                        { "data": "date" },
                        { "data": "threePhaseDuration" },
                        { "data": "threePhaseTimeDuration" },
                        { "data": "twoPhaseDuration" },
                        { "data": "twoPhaseTimeDuraton" },
                        { "data": "singlePhaseDuration" },
                        { "data": "singlePhaseTimeDuration" },
                        { "data": "ourtageDuration" },
                        { "data": "outageTimeDuration" },
                        { "data": "totalSupplyDuration" },
                        { "data": "remarks" }
            ],
            buttons: [
        {
            extend: 'excelHtml5',
            text: 'Excel',
            messageTop: 'The information in this table is copyright to Sirius Cybernetics Corp.'
        }
            ]
        });
    }

});

app.controller('logTrippingConyroller', function ($scope, $filter, $http) {

    $scope.logTrippingPDF = function () {
        debugger;
        $http.get('http://localhost:55130/Service1.svc/getTrippingLog/' + meterno + "/" + date).then(function (response) {
            debugger;
            var data = [];

            var temp1 = [

                    { text: '', style: 'tableHeader' },
                    { text: 'No.of Trippings during 3-Ph Supply Hrs', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: 'No.of Trippings during 2-Ph Supply Hrs', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: 'No.of Trippings during 1-Ph Supply Hrs', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: 'No.of Trippings during Total Supply Hrs', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                    {}, { text: '', style: 'tableHeader', alignment: 'center' }
            ];

            var temp2 = [

                    { text: 'Date', style: 'tableHeader', alignment: 'center' },
                    { text: 'Total', style: 'tableHeader', alignment: 'center' },
                    { text: 'After Ignoring Tripping < 10 minutes', style: 'tableHeader', alignment: 'center' },
                     { text: 'Total', style: 'tableHeader', alignment: 'center' },
                    { text: 'After Ignoring Tripping < 10 minutes', style: 'tableHeader', alignment: 'center' },
                     { text: 'Total', style: 'tableHeader', alignment: 'center' },
                    { text: 'After Ignoring Tripping < 10 minutes', style: 'tableHeader', alignment: 'center' },
                     { text: 'Total', style: 'tableHeader', alignment: 'center' },
                    { text: 'After Ignoring Tripping < 10 minutes', style: 'tableHeader', alignment: 'center' },
                    { text: 'Remarks', style: 'tableHeader', alignment: 'center' }

            ];

            for (var i = 0; i < response.data.length; i++) {
                data[i] = new Array(10);
                data[i][0] = (response.data[i].date);
                data[i][1] = response.data[i].th_ph_tripping;
                data[i][2] = (response.data[i].th_ph_tripping_l10);
                data[i][3] = response.data[i].tw_ph_tripping;
                data[i][4] = (response.data[i].tw_ph_tripping_l10);
                data[i][5] = response.data[i].si_ph_tripping;
                data[i][6] = (response.data[i].si_ph_tripping_l10);
                data[i][7] = response.data[i].tot_ph_tripping;
                data[i][8] = (response.data[i].tot_ph_tripping_l10);
                data[i][9] = response.data[i].remarks;
            }
            debugger;
            data.splice(0, 0, temp1);
            data.splice(1, 0, temp2);
            console.log(data);

            var date1 = new Date(date), locale = "en-us", month = date1.toLocaleString(locale, { month: "long" });
            var curDate = new Date().toLocaleString();
            var dd = {
                pageMargins: [40, 60, 40, 30],
                header: {
                    margin: 10,
                    columns: [
                        {
                            margin: [0, 20, 10, 0],
                            alignment: 'right',
                            fit: [100, 100],
                            image: 'building',

                        }
                    ]
                },
                content: [
                    { text: 'Feeder Tripping Details for the month ' + month, alignment: 'center', bold: true, fontSize: 11, margin: [0, 0, 0, 0] },
                    { text: 'Date: ' + curDate, alignment: 'right', fontSize: 8, margin: [0, 10, 0, 10] },
                    {
                        style: 'tableExample',
                        color: '#444',
                        table: {
                            widths: ['auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
                            //widths: ['auto', 'auto', 'auto'],
                            headerRows: 2,
                            // keepWithHeaderRows: 1,
                            body: data
                        }
                    },

                ],

                styles: {
                    header: {
                        fontSize: 8,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                    subheader: {
                        fontSize: 6,
                        bold: true,
                        margin: [0, 10, 0, 5]
                    },
                    tableExample: {
                        margin: [0, 5, 0, 15],
                        fontSize: 9,
                    },
                    tableHeader: {
                        bold: true,
                        fontSize: 9,
                        color: 'black',
                        fillColor: '#ee9800',
                    }
                },
                images: {
                    building: image_pdf
                },
                defaultStyle: {
                    // alignment: 'justify'
                }

            }
            pdfMake.createPdf(dd).download("logTripping_" + date + ".pdf");
        });
    };

    $('#txtLogTripping').focusin(function () {
        $('.ui-datepicker-calendar').css("display", "none");
    });

    $scope.txtLogTrippingDate = new Date($scope.txtLogTrippingDate);

    var date = $filter('date')(new Date(pdate), 'MMMM yyyy');
    $scope.txtLogTrippingDate = date;

    $scope.trippingDateOpts = {
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,
        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            seriesCounter = 0;
            date = $filter('date')(new Date(iYear, iMonth), 'MMMM yyyy');
            //getLogSupplyTable($filter('date')(new Date(iYear, iMonth), 'MMMM yyyy'));
        },
        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            }
        }
    };

    getLogTrippingTable(date);

    $scope.logTrippingClick = function () {
        getLogTrippingTable(date);
    }

    function getLogTrippingTable(date) {
        $("#tblLogTripping").DataTable({
            "bDestroy": true,
            "processing": true,
            "paging": false,
            "bAutoWidth": false,
            dom: 'lBfrtip',
            // Column definitions
            "ordering": false,
            "order": [],
            "columnDefs": [{
                "targets": '0',
                "orderable": false,
            }],
            "ajax": {
                "url": "http://localhost:55130/Service1.svc/getTrippingLog/" + meterno + "/" + date,
                "dataSrc": ""
            },
            "columns": [
                        { "data": "date" },
                        { "data": "th_ph_tripping" },
                        { "data": "th_ph_tripping_l10" },
                        { "data": "tw_ph_tripping" },
                        { "data": "tw_ph_tripping_l10" },
                        { "data": "si_ph_tripping" },
                        { "data": "si_ph_tripping_l10" },
                        { "data": "tot_ph_tripping" },
                        { "data": "tot_ph_tripping_l10" },
                        { "data": "remarks" },
            ],
            buttons: [
     {
         extend: 'excelHtml5',
         text: 'Excel',
         messageTop: 'The information in this table is copyright to Sirius Cybernetics Corp.'
     }, {
         extend: 'pdfHtml5',
         messageTop: 'The information in this table is copyright to Sirius Cybernetics Corp.',
         messageBottom: null,
         filename: function () {
             var d = new Date();
             var n = d.getTime();
             return 'myfile' + n;
         },
         title: 'Threephase',
         customize: function (doc) {
             doc.content.splice(1, 0, {
                 margin: [0, 0, 0, 12],
                 alignment: 'right',
             });
             doc.styles['tableHeader'] = {
                 bold: true,
                 fontSize: 11,
                 color: 'white',
                 fillColor: '#ee9800 ',
                 alignment: 'left'

             };
         }
     }
            ]
        });
    }

});

app.directive('hcChart', function () {
    return {
        restrict: 'E',
        template: '<div></div>',
        scope: {
            options: '='
        },
        link: function (scope, element) {
            Highcharts.chart(element[0], scope.options);
        }
    };
});

app.controller('voltageGraphController', function ($scope, $filter) {
    var seriesOptions = [],
          seriesCounter = 0,
          names = ['vr', 'vy', 'vb'];

    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        Highcharts.stockChart('voltage', {

            rangeSelector: {
                selected: 4
            },

            chart: {
                type: 'line',
                backgroundColor: {
                    linearGradient: [0, 0, 250, 500],
                    stops: [
                        [0, 'rgb(48, 96, 48)'],
                       [1, 'rgb(0, 0, 0)']
                    ]
                },
                borderColor: '#000000',
                borderWidth: 2,
                className: 'dark-container',
                plotBackgroundColor: 'rgba(255, 255, 255, .1)',
                plotBorderColor: '#CCCCCC',
                plotBorderWidth: 1

            },

            title: {
                text: '',
                style: {
                    color: '#FFFFFF',
                    font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                },

                title: {
                    text: 'Voltage',
                    style: {
                        color: '#FFFFFF',
                        font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                    }
                },
                plotBands: [{
                    from: 0,
                    to: 0,
                    color: 'rgba(68, 170, 213, 0.2)',
                    label: {
                        text: 'Date Time',
                        style: {
                            color: '#FFFFFF',
                            font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                        }
                    }
                }]
            },

            xAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                }
            },
            scrollbar: {
                barBackgroundColor: 'gray',
                barBorderRadius: 7,
                barBorderWidth: 0,
                buttonBackgroundColor: 'gray',
                buttonBorderWidth: 0,
                buttonBorderRadius: 7,
                trackBackgroundColor: 'none',
                trackBorderWidth: 1,
                trackBorderRadius: 8,
                trackBorderColor: '#FFFFFF'
            },
            series: seriesOptions
        });
    }

    $.each(names, function (i, name) {

        $.getJSON('http://localhost:55130/Service1.svc/getVoltageGraph/' + meterno + "/" + name, function (vyData) {

            var vData;
            var vy_data = '[';
            var color;

            $.each(vyData, function (j, vd) {
                if (j === vyData.length) {
                    return false;
                }

                if (name === 'vr') {
                    vData = Number(vyData[j].vr);
                    color = '#FF0000';
                }
                else if (name === 'vy') {
                    vData = Number(vyData[j].vy);
                    color = '#FFFF00';
                }
                else if (name === 'vb') {
                    vData = Number(vyData[j].vb);
                    color = '#0000FF';
                }

                if (j != vyData.length - 1) {
                    if (vData === "") {
                        vy_data = vy_data + '[' + Number(vyData[j].time) + ',0],';
                    }
                    else {
                        vy_data = vy_data + '[' + Number(vyData[j].time) + ',' + vData + '],';
                    }
                }

                else {
                    if (vData === "") {
                        vy_data = vy_data + '[' + Number(vyData[j].time) + ',0';
                    }
                    else {
                        vy_data = vy_data + '[' + Number(vyData[j].time) + ',' + vData + ']';
                    }
                }

            })

            vy_data = vy_data + ']';

            var newJson = vy_data.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
            newJson = newJson.replace(/'/g, '"');

            var data = JSON.parse(newJson);

            seriesOptions[i] = {
                name: name,
                data: data,
                color: color,
                dataGrouping: {
                    enabled: false
                },
                tooltip: {
                    valueDecimals: 2,
                    valueSuffix: 'Voltage'
                }

            };

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
            }
        });
    });
});

app.controller('currentGraphController', function ($scope, $filter) {
    var seriesOptions = [],
       seriesCounter = 0,
       names = ['ir', 'iy', 'ib'];

    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        Highcharts.stockChart('current', {

            rangeSelector: {
                selected: 4
            },

            chart: {
                type: 'line',
                backgroundColor: {
                    linearGradient: [0, 0, 250, 500],
                    stops: [
                        [0, 'rgb(48, 96, 48)'],
                       [1, 'rgb(0, 0, 0)']
                    ]
                },
                borderColor: '#000000',
                borderWidth: 2,
                className: 'dark-container',
                plotBackgroundColor: 'rgba(255, 255, 255, .1)',
                plotBorderColor: '#CCCCCC',
                plotBorderWidth: 1

            },

            title: {
                text: '',
                style: {
                    color: '#FFFFFF',
                    font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                },

                title: {
                    text: 'Current',
                    style: {
                        color: '#FFFFFF',
                        font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                    }
                },
                plotBands: [{
                    from: 0,
                    to: 0,
                    color: 'rgba(68, 170, 213, 0.2)',
                    label: {
                        text: 'Date Time',
                        style: {
                            color: '#FFFFFF',
                            font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                        }
                    }
                }]
            },

            xAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                }
            },
            scrollbar: {
                barBackgroundColor: 'gray',
                barBorderRadius: 7,
                barBorderWidth: 0,
                buttonBackgroundColor: 'gray',
                buttonBorderWidth: 0,
                buttonBorderRadius: 7,
                trackBackgroundColor: 'none',
                trackBorderWidth: 1,
                trackBorderRadius: 8,
                trackBorderColor: '#FFFFFF'
            },
            series: seriesOptions
        });
    }

    $.each(names, function (i, name) {

        $.getJSON('http://localhost:55130/Service1.svc/getCurrentGraph/' + meterno + "/" + name, function (cData) {

            var cData_status;
            var c_data = '[';
            var color;

            $.each(cData, function (j, vd) {

                if (j === cData.length) {
                    return false;
                }

                if (name === 'ir') {
                    cData_status = Number(cData[j].Ir);
                    color = '#FF0000';
                }
                else if (name === 'iy') {
                    cData_status = Number(cData[j].Iy);
                    color = '#FFFF00';
                }
                else if (name === 'ib') {
                    cData_status = Number(cData[j].Ib);
                    color = '#0000FF';
                }

                if (j != cData.length - 1) {
                    if (cData_status === "") {
                        c_data = c_data + '[' + Number(cData[j].time) + ',0],';
                    }
                    else {
                        c_data = c_data + '[' + Number(cData[j].time) + ',' + cData_status + '],';
                    }
                }

                else {
                    if (cData_status === "") {
                        c_data = c_data + '[' + Number(cData[j].time) + ',0';
                    }
                    else {
                        c_data = c_data + '[' + Number(cData[j].time) + ',' + cData_status + ']';

                    }
                }

            })

            c_data = c_data + ']';

            var newJson = c_data.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
            newJson = newJson.replace(/'/g, '"');

            var data = JSON.parse(newJson);


            seriesOptions[i] = {
                name: name,
                data: data,
                color: color,
                dataGrouping: {
                    enabled: false
                },
                tooltip: {
                    valueDecimals: 2,
                    valueSuffix: 'Current'
                }

            };

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
            }
        });
    });
});

app.controller('powerGraphController', function ($scope, $filter) {
    var seriesOptions = [],
         seriesCounter = 0,
         names = ['ir', 'iy', 'ib'];

    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        Highcharts.stockChart('power', {

            rangeSelector: {
                selected: 4
            },

            chart: {
                type: 'line',
                backgroundColor: {
                    linearGradient: [0, 0, 250, 500],
                    stops: [
                        [0, 'rgb(48, 96, 48)'],
                       [1, 'rgb(0, 0, 0)']
                    ]
                },
                borderColor: '#000000',
                borderWidth: 2,
                className: 'dark-container',
                plotBackgroundColor: 'rgba(255, 255, 255, .1)',
                plotBorderColor: '#CCCCCC',
                plotBorderWidth: 1

            },

            title: {
                text: '',
                style: {
                    color: '#FFFFFF',
                    font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                },

                title: {
                    text: 'Current',
                    style: {
                        color: '#FFFFFF',
                        font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                    }
                },
                plotBands: [{
                    from: 0,
                    to: 0,
                    color: 'rgba(68, 170, 213, 0.2)',
                    label: {
                        text: 'Date Time',
                        style: {
                            color: '#FFFFFF',
                            font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                        }
                    }
                }]
            },

            xAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                }
            },
            scrollbar: {
                barBackgroundColor: 'gray',
                barBorderRadius: 7,
                barBorderWidth: 0,
                buttonBackgroundColor: 'gray',
                buttonBorderWidth: 0,
                buttonBorderRadius: 7,
                trackBackgroundColor: 'none',
                trackBorderWidth: 1,
                trackBorderRadius: 8,
                trackBorderColor: '#FFFFFF'
            },
            series: seriesOptions
        });
    }



    $.getJSON('http://localhost:55130/Service1.svc/getPowerGraph/' + meterno, function (pData) {

        var pData_status;
        var p_data = '[';
        var color;
        $.each(pData, function (j, vd) {

            if (j === pData.length) {
                return false;
            }


            pData_status = Number(pData[j].pf);
            color = '#FF0000';


            if (j != pData.length - 1) {
                if (pData_status === "") {
                    p_data = p_data + '[' + Number(pData[j].time) + ',0],';
                }
                else {
                    p_data = p_data + '[' + Number(pData[j].time) + ',' + pData_status + '],';
                }
            }

            else {
                if (pData_status === "") {
                    p_data = p_data + '[' + Number(pData[j].time) + ',0';
                }
                else {
                    p_data = p_data + '[' + Number(pData[j].time) + ',' + pData_status + ']';
                }
            }

        })

        p_data = p_data + ']';
        var newJson = p_data.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
        newJson = newJson.replace(/'/g, '"');

        var data = JSON.parse(newJson);


        seriesOptions[0] = {
            name: 'PF',
            data: data,
            color: color,
            dataGrouping: {
                enabled: false
            },
            tooltip: {
                valueDecimals: 2,
                valueSuffix: 'Power Factor'
            }

        };

        // As we're loading the data asynchronously, we don't know what order it will arrive. So
        // we keep a counter and create the chart when all the data is loaded.

        createChart();

    });
});

app.controller('energyGraphController', function ($scope, $filter) {
    var seriesOptions = [],
          seriesCounter = 0,
          names = ['kwh', 'kvah', 'kvarh_lg', 'kvarh_ld'];

    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        Highcharts.stockChart('energy', {

            rangeSelector: {
                selected: 4
            },

            chart: {
                type: 'line',
                backgroundColor: {
                    linearGradient: [0, 0, 250, 500],
                    stops: [
                        [0, 'rgb(48, 96, 48)'],
                       [1, 'rgb(0, 0, 0)']
                    ]
                },
                borderColor: '#000000',
                borderWidth: 2,
                className: 'dark-container',
                plotBackgroundColor: 'rgba(255, 255, 255, .1)',
                plotBorderColor: '#CCCCCC',
                plotBorderWidth: 1

            },

            title: {
                text: '',
                style: {
                    color: '#FFFFFF',
                    font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                },

                title: {
                    text: 'Energy',
                    style: {
                        color: '#FFFFFF',
                        font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                    }
                },
                plotBands: [{
                    from: 0,
                    to: 0,
                    color: 'rgba(68, 170, 213, 0.2)',
                    label: {
                        text: 'Date Time',
                        style: {
                            color: '#FFFFFF',
                            font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                        }
                    }
                }]
            },

            xAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                }
            },
            scrollbar: {
                barBackgroundColor: 'gray',
                barBorderRadius: 7,
                barBorderWidth: 0,
                buttonBackgroundColor: 'gray',
                buttonBorderWidth: 0,
                buttonBorderRadius: 7,
                trackBackgroundColor: 'none',
                trackBorderWidth: 1,
                trackBorderRadius: 8,
                trackBorderColor: '#FFFFFF'
            },
            series: seriesOptions
        });
    }

    $.each(names, function (i, name) {
        $.getJSON('http://localhost:55130/Service1.svc/getEnergyGraph/' + meterno + "/" + name, function (eData) {
            var eData_status;
            var e_data = '[';
            var color;

            $.each(eData, function (j, vd) {

                if (j === eData.length) {
                    return false;
                }

                if (name === 'kwh') {
                    eData_status = Number(eData[j].kwh);
                    color = '#FF0000';
                }
                else if (name === 'kvah') {
                    eData_status = Number(eData[j].kvah);
                    color = '#FFFF00';
                }
                else if (name === 'kvarh_lg') {
                    eData_status = Number(eData[j].kvarh_lag);
                    color = '#0000FF';
                }
                else if (name === 'kvarh_ld') {
                    eData_status = Number(eData[j].kvarh_lead);
                    color = '#009900'
                }

                if (j != eData.length - 1) {
                    if (eData_status === "") {
                        e_data = e_data + '[' + Number(eData[j].time) + ',0],';
                    }
                    else {
                        e_data = e_data + '[' + Number(eData[j].time) + ',' + eData_status + '],';
                    }
                }

                else {
                    if (eData_status === "") {
                        e_data = e_data + '[' + Number(eData[j].time) + ',0';
                    }
                    else {
                        e_data = e_data + '[' + Number(eData[j].time) + ',' + eData_status + ']';

                    }
                }

            })

            e_data = e_data + ']';
            var newJson = e_data.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
            newJson = newJson.replace(/'/g, '"');

            var data = JSON.parse(newJson);


            seriesOptions[i] = {
                name: name,
                data: data,
                color: color,
                dataGrouping: {
                    enabled: false
                },
                tooltip: {
                    valueDecimals: 2,
                    valueSuffix: 'Energy'
                }

            };

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
                //alert($scope.zone);
                $('#danceforme').hide();
            }
        });
    });
});

app.controller('loadGraphController', function ($scope) {
    var seriesOptions = [],
        seriesCounter = 0,
        names = ['kw', 'kva'];

    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        Highcharts.stockChart('load', {

            rangeSelector: {
                selected: 4
            },

            chart: {
                type: 'line',
                backgroundColor: {
                    linearGradient: [0, 0, 250, 500],
                    stops: [
                        [0, 'rgb(48, 96, 48)'],
                       [1, 'rgb(0, 0, 0)']
                    ]
                },
                borderColor: '#000000',
                borderWidth: 2,
                className: 'dark-container',
                plotBackgroundColor: 'rgba(255, 255, 255, .1)',
                plotBorderColor: '#CCCCCC',
                plotBorderWidth: 1

            },

            title: {
                text: '',
                style: {
                    color: '#FFFFFF',
                    font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                },

                title: {
                    text: 'LOAD',
                    style: {
                        color: '#FFFFFF',
                        font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                    }
                },
                plotBands: [{
                    from: 0,
                    to: 0,
                    color: 'rgba(68, 170, 213, 0.2)',
                    label: {
                        text: 'Date Time',
                        style: {
                            color: '#FFFFFF',
                            font: 'bold 16px "Trebuchet MS", Segoe UI, sans-serif'
                        }
                    }
                }]
            },

            xAxis: {
                labels: {
                    style: {
                        color: '#FFFFFF'
                    }
                }
            },
            scrollbar: {
                barBackgroundColor: 'gray',
                barBorderRadius: 7,
                barBorderWidth: 0,
                buttonBackgroundColor: 'gray',
                buttonBorderWidth: 0,
                buttonBorderRadius: 7,
                trackBackgroundColor: 'none',
                trackBorderWidth: 1,
                trackBorderRadius: 8,
                trackBorderColor: '#FFFFFF'
            },
            series: seriesOptions
        });
    }

    $.each(names, function (i, name) {
        $.getJSON('http://localhost:55130/Service1.svc/getLoadGraph/' + meterno + "/" + name, function (lData) {
            var lData_status;
            var l_data = '[';
            var color;

            $.each(lData, function (j, vd) {

                if (j === lData.length) {
                    return false;
                }

                if (name === 'kw') {
                    lData_status = Number(lData[j].kw);
                    color = '#FF0000';
                }
                else if (name === 'kva') {
                    lData_status = Number(lData[j].kva);
                    color = '#FFFF00';
                }

                if (j != lData.length - 1) {
                    if (lData_status === "") {
                        l_data = l_data + '[' + Number(lData[j].time) + ',0],';
                    }
                    else {
                        l_data = l_data + '[' + Number(lData[j].time) + ',' + lData_status + '],';
                    }
                }

                else {
                    if (lData_status === "") {
                        l_data = l_data + '[' + Number(lData[j].time) + ',0';
                    }
                    else {
                        l_data = l_data + '[' + Number(lData[j].time) + ',' + lData_status + ']';

                    }
                }

            })

            l_data = l_data + ']';
            var newJson = l_data.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
            newJson = newJson.replace(/'/g, '"');

            var data = JSON.parse(newJson);


            seriesOptions[i] = {
                name: name,
                data: data,
                color: color,
                dataGrouping: {
                    enabled: false
                },
                tooltip: {
                    valueDecimals: 2,
                    valueSuffix: 'Load'
                }

            };

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
            }
        });
    });
});

app.controller('monthSupplyController', function ($scope, $filter) {
    var seriesOptions = [],
      seriesCounter = 0, names = ['three', 'two', 'one', 'powerfail', 'notcomm'];
    var year, month;
    var date = $filter('date')(new Date(pdate), 'MMMM yyyy');
    month = $filter('date')(new Date(pdate), 'M');
    year = $filter('date')(new Date(pdate), 'yyyy');
    $scope.txtMomthlySupplyDate = date;

    getMontlySupplyGraph(date);

    $('#txtMonthDate').focusin(function () {
        $('.ui-datepicker-calendar').css("display", "none");
    });

    $scope.opts = {
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,
        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            seriesCounter = 0;
            date = $filter('date')(new Date(iYear, iMonth), 'MMMM yyyy');
            month = $filter('date')(new Date(iYear, iMonth), 'M');
            year = $filter('date')(new Date(iYear, iMonth), 'yyyy');
        },
        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            }
        }
    };

    $scope.monthlySupplyClick = function () {
        getMontlySupplyGraph(date);
    }


    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        Highcharts.chart('container', {

            chart: {
                type: 'column'
            },
            title: {
                text: 'Monthly Average Rainfall'
            },
            subtitle: {
                text: 'Source: WorldClimate.com'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    day: '%e'
                },

                title: {
                    text: '<b>Number of Days</b>'
                },
                tickInterval: 24 * 3600 * 1000,
                startOnTick: false,
                showFirstLabel: true
            },
            yAxis: {
                type: 'linear',
                labels: {
                    formatter: function () {
                        var seconds = (this.value / 1000) | 0;
                        this.value -= seconds * 1000;

                        var minutes = (seconds / 60) | 0;
                        seconds -= minutes * 60;

                        var hours = (minutes / 60) | 0;
                        minutes -= hours * 60;
                        return hours;
                    }
                },
                title: {
                    text: '<b>Number of Hours</b>'
                },
                min: 0,
                max: 24 * 3600 * 1000,
                tickInterval: 3 * 3600 * 1000,
                startOnTick: false,
                showFirstLabel: false
            },
            tooltip: {
                headerFormat: '<span style = "font-size:10px"><center><b>Day : {point.key}</b></center></span><table>',
                pointFormat: '<tr><td style = "color:{series.color};padding:0"><b>{series.name}:&nbsp &nbsp </b></td>' +
                   '<td style = "padding:0"><b>{point.y:%H:%M} (HH:MM)</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: seriesOptions
        });
    }

    function getMontlySupplyGraph(date) {
        $.each(names, function (i, name) {
            $.getJSON('http://localhost:55130/Service1.svc/getMonthlySupply/' + meterno + "/" + date + "/" + name, function (lData) {
                //var temp = "[1517423400000,83984000],[1517509800000,80384000],[1517596200000,8384000],[1517682600000,44384000],[1517769000000,44384000],[1517855400000,0],[1517941800000,44388000],[1518028200000,44784000],[1518114600000,44394000],[1518201000000,44388000],[1518287400000,0],[1518373800000,44314000],[1518460200000,44385000],[1518546600000,0],[1518633000000,44304000]"
                var temp = "83984000, 80384000, 8384000,44384000,44384000,0,44388000,44784000,44394000,44388000,0,44314000,44385000,0,44304000";
                var temp1 = "[83984000, 80384000, 8384000,44384000,44384000,0,44388000,44784000,44394000,44388000,0,44314000,44385000,0,44304000]";
                var lData_status;
                var l_data = '[';
                var color, titleName;

                if (name === "three") {
                    $scope.threePhaseDuration = lData[0].th_ph_Dur;
                    l_data = lData[0].th_Ph;
                    color = '#1b5e20';
                    titleName = 'Three Phase Running'
                }
                else if (name === "two") {
                    $scope.twoPhaseDuration = lData[0].tw_ph_Dur;
                    l_data = lData[0].tw_ph;
                    color = '#e65100';
                    titleName = 'Two Phase Running';
                }
                else if (name === "one") {
                    $scope.singlePhaseDuration = lData[0].si_ph_Dur;
                    l_data = lData[0].si_ph;
                    color = '#ffd600';
                    titleName = 'Single Phase Running';
                }
                else if (name === "powerfail") {
                    $scope.powerFailDuration = lData[0].powerFail_Dur;
                    l_data = lData[0].powerFail;
                    color = '#b71c1c';
                    titleName = 'Power Failure';
                }
                else if (name === "notcomm") {
                    $scope.notCommunicationDuration = lData[0].not_comm_Dur;
                    l_data = lData[0].notCom;
                    color = '#afaaaa';
                    titleName = 'Meter Not Responding';
                }

                var newJson = l_data.replace(/([a-zA-Z0-9]+?):/g, '"$1":');
                newJson = newJson.replace(/'/g, '"');
                var data = JSON.parse(newJson);

                seriesOptions[i] = {
                    name: titleName,
                    data: data,
                    color: color,
                    pointStart: Date.UTC(year, month - 1, 1),
                    pointInterval: 24 * 3600 * 1000
                };

                // As we're loading the data asynchronously, we don't know what order it will arrive. So
                // we keep a counter and create the chart when all the data is loaded.
                seriesCounter += 1;

                if (seriesCounter === names.length) {
                    createChart();
                }
            });
        });
    }
});

app.controller('instantsController', function ($scope, $timeout, $cookies, ngToast) {
    var redData, yellowData, blueData, checkBoxValue, readDate, innerCount = 0;
    $cookies.put('readDate', readDate);

    $scope.itens = [
       { title: "R-Phase", checked: true },
       { title: "Y-Phase", checked: false },
       { title: "B-Phase", checked: false },
    ];
    $cookies.put("chbkValue", "R-Phase");
    getGaugeData("R-Phase");

    $scope.updateSelection = function (position, itens, title) {
        $cookies.put("chbkValue", title);
        getGaugeData(title);
        angular.forEach(itens, function (subscription, index) {
            if (position != index)
                subscription.checked = false;

            $scope.selected = title;
        }
        );
    };
    var count = 0;
    $scope.intervalFunction = function () {
        $timeout(function () {
            getGaugeData($cookies.get("chbkValue"));
            count++;
            if (innerCount <= 1) {
                $scope.intervalFunction();
            }
            else {
                ngToast.create({
                    className: 'success',
                    content: ' Data Refreshed <b>Successfully</b>',
                    timeout: 5000
                });
            }

        }, 10000)
    };

    $scope.intervalFunction();

    function createRedChart(id, zero, one, data) {
        Highcharts.chart(id, {

            chart: {
                type: 'gauge'
            },

            title: {
                text: ''
            },
            pane: {
                startAngle: -150,
                endAngle: 150,

                background: [{
                    backgroundColor: {
                        radialGradient: { cx: 0.5, cy: 0.5, r: 0.5 }, //
                        stops: [
                                   [0, zero], //CCD5DE
                                   [1, one]  //002E59
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '105%' //107
                },
                {
                    backgroundColor: '#black',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 12,
                title: {
                    text: '<br/><span style="font-size:20px">kV</span>',
                    y: 60
                }
            },
            plotOptions: {
                gauge: {
                    dial: {
                        radius: '85%',
                        backgroundColor: 'black',
                        borderColor: 'black',
                        borderWidth: 1,
                        baseWidth: 5,
                        topWidth: 1,
                        baseLength: '10%', // of radius --before90%
                        rearLength: '10%' //--50%
                    }
                }
            },

            series: [{
                name: 'Voltage',
                data: [data],
                tooltip: {
                    valueDecimals: 2,
                    valueSuffix: ' kV'
                },
                dataLabels: {
                    borderRadius: 0,
                    borderWidth: 0,
                }
            }]

        }
        );
    }
    function createYellowChart(id, zero, one, data) {
        Highcharts.chart(id, {

            chart: {
                type: 'gauge'
            },

            title: {
                text: ''
            },
            pane: {
                startAngle: -150,
                endAngle: 150,

                background: [{
                    backgroundColor: {
                        radialGradient: { cx: 0.5, cy: 0.5, r: 0.5 }, //
                        stops: [
                                   [0, zero], //CCD5DE
                                   [1, one]  //002E59
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '105%' //107
                },
                {
                    backgroundColor: '#black',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 300,
                title: {
                    text: '<br/><span style="font-size:20px">A</span>',
                    y: 60
                }
            },
            plotOptions: {
                gauge: {
                    dial: {
                        radius: '85%',
                        backgroundColor: 'black',
                        borderColor: 'black',
                        borderWidth: 1,
                        baseWidth: 5,
                        topWidth: 1,
                        baseLength: '10%', // of radius --before90%
                        rearLength: '10%' //--50%
                    }
                }
            },

            series: [{
                name: 'Current',
                data: [data],
                tooltip: {
                    valueDecimals: 2,
                    valueSuffix: ' A'
                },
                dataLabels: {
                    borderRadius: 0,
                    borderWidth: 0,
                }
            }]

        }
        );
    }
    function createBlueChart(id, zero, one, data) {
        Highcharts.chart(id, {

            chart: {
                type: 'gauge'
            },

            title: {
                text: ''
            },
            pane: {
                startAngle: -150,
                endAngle: 150,

                background: [{
                    backgroundColor: {
                        radialGradient: { cx: 0.5, cy: 0.5, r: 0.5 }, //
                        stops: [
                                   [0, zero], //CCD5DE
                                   [1, one]  //002E59
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '105%' //107
                },
                {
                    backgroundColor: '#black',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: -1,
                max: 1
            },
            plotOptions: {
                gauge: {
                    dial: {
                        radius: '85%',
                        backgroundColor: 'black',
                        borderColor: 'black',
                        borderWidth: 1,
                        baseWidth: 5,
                        topWidth: 1,
                        baseLength: '10%', // of radius --before90%
                        rearLength: '10%' //--50%
                    }
                }
            },

            series: [{
                name: 'Power Factor',
                data: [data]
            }]

        }
        );
    }

    function getGaugeData(checkItem) {
        var redData, yellowData, blueData;
        $.getJSON('http://localhost:55130/Service1.svc/getInstants/' + meterno, function (Idata) {
            //console.log(Idata);
            $scope.vr = Idata[0].vr;
            $scope.vy = Idata[0].vy;
            $scope.vb = Idata[0].vb;
            $scope.ir = Idata[0].ir;
            $scope.iy = Idata[0].iy;
            $scope.ib = Idata[0].ib;
            $scope.qr = Idata[0].qr;
            $scope.qy = Idata[0].qy;
            $scope.qb = Idata[0].qb;
            if (Idata[0].meter_date != $cookies.get('readDate')) {
                readDate = Idata[0].meter_date;
                $cookies.put('readDate', readDate);
                innerCount++;
            }
            $scope.readDate = Idata[0].meter_date;
            $scope.kw = Idata[0].kw;
            $scope.kvar = Idata[0].kvar;
            $scope.kva = Idata[0].kva;
            $scope.freq = Idata[0].freq;
            $scope.cumkWh = Idata[0].cum_kwh;
            $scope.cumkVArh_Lag = Idata[0].cum_kvarh_lg;
            $scope.cumkVArh_Lead = Idata[0].cum_kvarh_ld;
            $scope.CumkVAh = Idata[0].cum_kvah;

            if (checkItem === "R-Phase") {
                redData = Idata[0].vr;
                yellowData = Idata[0].ir;
                blueData = Idata[0].qr;
            }

            else if (checkItem === "Y-Phase") {
                redData = Idata[0].vy;
                yellowData = Idata[0].iy;
                blueData = Idata[0].qy;
            }

            else if (checkItem === "B-Phase") {
                redData = Idata[0].vb;
                yellowData = Idata[0].ib;
                blueData = Idata[0].qb;
            }
            createRedChart('red', '#FFFFFF', '#FF4747', Number(redData));
            createYellowChart('yellow', '#FFFFFF', '#FFFF19', Number(yellowData));
            createBlueChart('blue', '#FFFFFF', '#1975FF', Number(blueData));
        })

    }
});

app.controller('AppController', function ($scope) {
    //html2canvas(document.querySelector("#example")).then(function (canvas) {
    //    debugger;
    //    var data = canvas.toDataURL();
    //    var docDefinition = {
    //        content: [{
    //            image: data,
    //            width: 500,
    //        }]
    //    };
    //    pdfMake.createPdf(docDefinition).download("Score_Details.pdf");
    //});

    //html2canvas(document.getElementById('example'), {
    //    onrendered: function (canvas) {
    //        var data = canvas.toDataURL();
    //        var docDefinition = {
    //            content: [{
    //                image: data,
    //                width: 500,
    //            }]
    //        };
    //        pdfMake.createPdf(docDefinition).download("Score_Details.pdf");
    //    }
    //});
})