﻿angular.module('myApp')
.factory('accessFac', function () {
    var obj = {}
    this.access = false;
    obj.getPermission = function () {    //set the permission to true
        this.access = true;
    }
    obj.checkPermission = function () {
        return this.access;             //returns the users permission level 
    }
    return obj;
})
.controller('homeController', function (accessFac, $filter, zoneService, circleService, mlaService, divService, subdivService, jenService, pieService, subtationService, phaseService, $scope, uiGridConstants, $http, $window, $uibModal, $rootScope, $cookies) {

    $rootScope.data = "";

    $scope.showThreePhase = false;
    $scope.showTwoPhase = false;
    $scope.showSinglePhase = false;
    $scope.today = new Date();

    var yes = new Date();
    var dd = yes.getDate() - 1;
    var mm = yes.getMonth();
    var yy = yes.getFullYear();
    $scope.txtDate = new Date(yy, mm, dd);
    $scope.dateOptions = {
        formatDay: 'dd',
        formatMonth: 'MM',
        formatYear: 'yyyy',
        maxDate: new Date(yy, mm, dd),
        showWeeks: false
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.format = 'dd-MM-yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.popup1 = {
        opened: false
    };


    zoneService.drpzone().then(function (zonedata) {
        $scope.zones = zonedata.data;
        $scope.dzone = $scope.zones[0];
        var zoneval = $scope.dzone.zone;
        circleService.drpcircle(zoneval).then(function (circledata) {
            $scope.circles = circledata.data;
            $scope.dcircle = $scope.circles[0];
            var circleval = $scope.dcircle.circle;
            mlaService.drpmla(zoneval, circleval).then(function (mladata) {
                $scope.mlas = mladata.data;
                $scope.dmla = $scope.mlas[0];
                var mlaval = $scope.dmla.mla;
                divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                    $scope.divs = divdata.data;
                    $scope.ddiv = $scope.divs[0];
                    var divval = $scope.ddiv.div;
                    subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                        $scope.subdivs = subdivdata.data;
                        $scope.dsubdiv = $scope.subdivs[0];
                        var subdivval = $scope.dsubdiv.subdiv;
                        jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                            $scope.jens = jendata.data;
                            $scope.djen = $scope.jens[0];
                            var jenval = $scope.djen.jen;
                            pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                                GetPiechart(piedata.data[0]);
                                $scope.total = piedata.data[0].total;
                                $scope.threeph = piedata.data[0].threephase;
                                $scope.twoph = piedata.data[0].twophase;
                                $scope.singph = piedata.data[0].singlephase;
                                $scope.noph = piedata.data[0].nophase;
                                $scope.ncph = piedata.data[0].notcomm;
                                //var t_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                                //alert(t_3ph.toFixed(2));
                                $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                            })

                            subtationService.substations(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (subdata) {
                                $scope.data = {
                                    photos: subdata.data,
                                    photos3p: partition(subdata.data, (subdata.data.length) / 5)
                                };
                            })


                            //var pdate = '2017-12-17';




                        })
                    })
                })
            })
        })

        $scope.dateChange = function (urlDate) {
            debugger;
            //var estatus = 'Three Phase Running';
            $scope.threePhaseClicked(urlDate);
            $scope.ima = true;
            //urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
            //grid($scope, $http, uiGridConstants, $scope.dzone.zone, $scope.dcircle.circle, $scope.dmla.mla, $scope.ddiv.div, $scope.dsubdiv.subdiv, $scope.djen.jen, estatus, urlDate, phaseService, $window, $uibModal, $rootScope, $cookies);

        };

        $scope.threePhaseClicked = function (urlDate, status) {
            debugger;
            var estatus = status;
            $scope.ima = true;
            if (status === 'Three Phase Running') {
                $scope.showThreePhase = $scope.showThreePhase === false ? true : false;
            }
            else if (status === 'Two Phase Running') {
                $scope.showTwoPhase = $scope.showTwoPhase === false ? true : false;
            }

            else if (status === 'Single Phase Running') {
                $scope.showSinglePhase = $scope.showSinglePhase === false ? true : false;
            }

            urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');

            grid($scope, $http, uiGridConstants, $scope.dzone.zone, $scope.dcircle.circle, $scope.dmla.mla, $scope.ddiv.div, $scope.dsubdiv.subdiv, $scope.djen.jen, estatus, urlDate, phaseService, $window, $uibModal, $rootScope, $cookies);

        };

        $scope.GetCircle = function () {
            var zoneval = $scope.dzone.zone;
            circleService.drpcircle(zoneval).then(function (circledata) {
                $scope.circles = circledata.data;
                $scope.dcircle = $scope.circles[0];
                var circleval = $scope.dcircle.circle;
                mlaService.drpmla(zoneval, circleval).then(function (mladata) {
                    $scope.mlas = mladata.data;
                    $scope.dmla = $scope.mlas[0];
                    var mlaval = $scope.dmla.mla;
                    divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                        $scope.divs = divdata.data;
                        $scope.ddiv = $scope.divs[0];
                        var divval = $scope.ddiv.div;
                        subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                            $scope.subdivs = subdivdata.data;
                            $scope.dsubdiv = $scope.subdivs[0];
                            var subdivval = $scope.dsubdiv.subdiv;
                            jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                                $scope.jens = jendata.data;
                                $scope.djen = $scope.jens[0];
                                var jenval = $scope.djen.jen;
                                pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                                    GetPiechart(piedata.data[0]);
                                    $scope.total = piedata.data[0].total;
                                    $scope.threeph = piedata.data[0].threephase;
                                    $scope.twoph = piedata.data[0].twophase;
                                    $scope.singph = piedata.data[0].singlephase;
                                    $scope.noph = piedata.data[0].nophase;
                                    $scope.ncph = piedata.data[0].notcomm;
                                    $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                                })
                            })
                        })
                    })
                })
            })
        }

        $scope.GetMla = function () {
            var zoneval = $scope.dzone.zone;
            var circleval = $scope.dcircle.circle;
            mlaService.drpmla(zoneval, circleval).then(function (mladata) {
                //console.log(mladata.data);
                $scope.mlas = mladata.data;
                $scope.dmla = $scope.mlas[0];
                var mlaval = $scope.dmla.mla;
                divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                    //console.log(divdata.data);
                    $scope.divs = divdata.data;
                    $scope.ddiv = $scope.divs[0];
                    var divval = $scope.ddiv.div;
                    subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                        //console.log(subdivdata.data);
                        $scope.subdivs = subdivdata.data;
                        $scope.dsubdiv = $scope.subdivs[0];
                        var subdivval = $scope.dsubdiv.subdiv;
                        jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                            //console.log(jendata.data);
                            $scope.jens = jendata.data;
                            $scope.djen = $scope.jens[0];
                            var jenval = $scope.djen.jen;
                            pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                                //console.log(piedata.data);
                                GetPiechart(piedata.data[0]);
                                $scope.total = piedata.data[0].total;
                                $scope.threeph = piedata.data[0].threephase;
                                $scope.twoph = piedata.data[0].twophase;
                                $scope.singph = piedata.data[0].singlephase;
                                $scope.noph = piedata.data[0].nophase;
                                $scope.ncph = piedata.data[0].notcomm;
                                $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                            })
                        })
                    })
                })
            })
        }

        $scope.GetDiv = function () {
            var zoneval = $scope.dzone.zone;
            var circleval = $scope.dcircle.circle;
            var mlaval = $scope.dmla.mla;
            divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                //console.log(divdata.data);
                $scope.divs = divdata.data;
                $scope.ddiv = $scope.divs[0];
                var divval = $scope.ddiv.div;
                subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                    //console.log(subdivdata.data);
                    $scope.subdivs = subdivdata.data;
                    $scope.dsubdiv = $scope.subdivs[0];
                    var subdivval = $scope.dsubdiv.subdiv;
                    jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                        //console.log(jendata.data);
                        $scope.jens = jendata.data;
                        $scope.djen = $scope.jens[0];
                        var jenval = $scope.djen.jen;
                        pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                            //console.log(piedata.data);
                            GetPiechart(piedata.data[0]);
                            $scope.total = piedata.data[0].total;
                            $scope.threeph = piedata.data[0].threephase;
                            $scope.twoph = piedata.data[0].twophase;
                            $scope.singph = piedata.data[0].singlephase;
                            $scope.noph = piedata.data[0].nophase;
                            $scope.ncph = piedata.data[0].notcomm;
                            $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                        })
                    })
                })
            })
        }

        $scope.GetSubDiv = function () {
            var zoneval = $scope.dzone.zone;
            var circleval = $scope.dcircle.circle;
            var mlaval = $scope.dmla.mla;
            var divval = $scope.ddiv.div;
            subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                //console.log(subdivdata.data);
                $scope.subdivs = subdivdata.data;
                $scope.dsubdiv = $scope.subdivs[0];
                var subdivval = $scope.dsubdiv.subdiv;
                jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                    //console.log(jendata.data);
                    $scope.jens = jendata.data;
                    $scope.djen = $scope.jens[0];
                    var jenval = $scope.djen.jen;
                    pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                        //console.log(piedata.data);
                        GetPiechart(piedata.data[0]);
                        $scope.total = piedata.data[0].total;
                        $scope.threeph = piedata.data[0].threephase;
                        $scope.twoph = piedata.data[0].twophase;
                        $scope.singph = piedata.data[0].singlephase;
                        $scope.noph = piedata.data[0].nophase;
                        $scope.ncph = piedata.data[0].notcomm;
                        $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                    })
                })
            })
        }

        $scope.GetJen = function () {
            var zoneval = $scope.dzone.zone;
            var circleval = $scope.dcircle.circle;
            var mlaval = $scope.dmla.mla;
            var divval = $scope.ddiv.div;
            var subdivval = $scope.dsubdiv.subdiv;
            jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                //console.log(jendata.data);
                $scope.jens = jendata.data;
                $scope.djen = $scope.jens[0];
                var jenval = $scope.djen.jen;
                pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                    //console.log(piedata.data);
                    GetPiechart(piedata.data[0]);
                    $scope.total = piedata.data[0].total;
                    $scope.threeph = piedata.data[0].threephase;
                    $scope.twoph = piedata.data[0].twophase;
                    $scope.singph = piedata.data[0].singlephase;
                    $scope.noph = piedata.data[0].nophase;
                    $scope.ncph = piedata.data[0].notcomm;
                    $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                })
            })
        }

        $scope.GetPie = function () {
            var zoneval = $scope.dzone.zone;
            var circleval = $scope.dcircle.circle;
            var mlaval = $scope.dmla.mla;
            var divval = $scope.ddiv.div;
            var subdivval = $scope.dsubdiv.subdiv;
            var jenval = $scope.djen.jen;
            pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                console.log(piedata.data);
                GetPiechart(piedata.data[0]);
                $scope.total = piedata.data[0].total;
                $scope.threeph = piedata.data[0].threephase;
                $scope.twoph = piedata.data[0].twophase;
                $scope.singph = piedata.data[0].singlephase;
                $scope.noph = piedata.data[0].nophase;
                $scope.ncph = piedata.data[0].notcomm;
                $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
            })
        }



    });

    function GetPiechart(pdata) {
        Highcharts.chart('feedercontainer', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 60,
                    beta: 0
                }
            },
            title: {
                text: '',
                style: {
                    font: '300 2.56rem Roboto',
                    color: '#ee9800'
                },
                align: 'left',
                y: 45
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.point.name + '</b>: ' + this.y;
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 55,
                    dataLabels: {
                        enabled: false
                    },
                    //animation: false,
                    point: {
                        events: {
                            click: function () {
                                var options = this.options;
                                //location.href = 'frmSinglePhase.aspx?parameter=' + options.name
                            }
                        }
                    }
                }
            },
            series: [{
                name: 'Browser share',
                size: '115%',
                data: [{
                    name: 'Three Phase Running',
                    y: parseInt(pdata.threephase)
                }, {
                    name: 'Two Phase Running',
                    y: parseInt(pdata.twophase)
                }, {
                    name: 'Single Phase Running',
                    y: parseInt(pdata.singlephase)
                }, {
                    name: 'Power Failure',
                    y: parseInt(pdata.nophase)
                }, {
                    name: 'Not Communicated',
                    y: parseInt(pdata.notcomm)
                }]
            }]
        });

    }

    function partition(input, size) {
        var newArr = [];
        for (var i = 0; i < input.length; i += size) {
            newArr.push(input.slice(i, i + size));
        }
        return newArr;
    }

})

.controller('testController', function ($scope, $routeParams, $window) {
    debugger;
    $scope.testid = $window.substation;
    alert($scope.testid);
    //$scope.testid = $routeParams.id;
    //$scope.testid1 = $routeParams.id1;
});

function grid($scope, $http, uiGridConstants, zoneval, circleval, mlaval, divval, subdivval, jenval, estatus, pdate, phaseService, $window, $uibModal, $rootScope, $cookies) {

    $scope.gridOptions = {
        enableGridMenu: true,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: true,
        enableFiltering: true,
        columnDefs: [
            { field: 'SNo', enableColumnMenu: false },
            { field: 'Substation', enableColumnMenu: false },
            { field: 'FeederName', enableColumnMenu: false },
            { field: 'Type', enableColumnMenu: false },
            { field: 'Area', enableColumnMenu: false },
            {
                field: 'MeterNo', name: 'Hyperlink',
                cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.edit(row.entity)" > {{COL_FIELD}}</a> ',
                enableColumnMenu: false
            },
            { field: 'MF', enableColumnMenu: false },
            { field: 'RTC', enableColumnMenu: false },
            { field: 'Duration' }
        ]
    };


    if (estatus == 'Three Phase Running') {
        $scope.threePhasegridOptions = $scope.gridOptions;
    }

    else if (estatus == 'Two Phase Running') {
        $scope.twoPhasegridOptions = $scope.gridOptions;
    }

    else if (estatus == 'Single Phase Running') {
        $scope.singlePhasegridOptions = $scope.gridOptions;
    }

    $rootScope.data = $scope.gridOptions;

    $scope.edit = function (row) {
        //$uibModal.open({
        //    templateUrl: 'test.html',
        //    size: "lg",
        //    controller: function ($scope, $uibModalInstance) {
        //        $scope.gridOptions = $rootScope.data;
        //        $scope.test = row.Substation;
        //        $scope.ok = function () {
        //            $uibModalInstance.close();
        //        };

        //        $scope.cancel = function () {
        //            $uibModalInstance.dismiss('cancel');
        //        };
        //    },
        //    clickOutsideToClose: false
        //}).result.catch(function (resp) {
        //    if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1) throw resp;
        //});
        debugger;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("test.htm", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);
        $cookies.put('myFavorite', row.Substation);
        $popup.Name = row.Substation;
        $popup.Test = "test1";

    };

    phaseService.phases(zoneval, circleval, mlaval, divval, subdivval, jenval, estatus, pdate).then(function (phasedata) {
        console.log(phasedata);
        if (estatus == 'Three Phase Running') {
            $scope.threePhasegridOptions.data = phasedata.data;
        }

        else if (estatus == 'Two Phase Running') {
            $scope.twoPhasegridOptions.data = phasedata.data;
        }

        else if (estatus == 'Single Phase Running') {
            $scope.singlePhasegridOptions.data = phasedata.data;
        }

        $rootScope.data.data = phasedata.data;
        $scope.ima = false;
    })
}

