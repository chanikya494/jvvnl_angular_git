﻿angular.module('myApp')

.controller('reportController', function ($scope, $location) {
    $scope.clicked = function (status) {
        if (status === "Feederwise")
            $location.path('/FeederwiseZone/1');
        else if (status === "RuralTripping")
            $location.path('/TrippingZone/R');
        else if (status === "UrbanTripping")
            $location.path('/TrippingZone/U');
        else if (status === "IndustrialTripping")
            $location.path('/TrippingZone/I');
        else if (status === "WaterTripping")
            $location.path('/TrippingZone/W');
    }
})

.controller('FeederwiseZoneController', function ($routeParams, $scope, $location, $filter, $http, $cookies, $window) {
    var d = new Date();
    var newMonth = d.getMonth() - 1;
    d.setDate(1);
    d.setMonth(newMonth);
    var nDate = $filter('date')(d, 'MMMM yyyy')
    var sid = $routeParams.id
    if (sid === '1')
        $scope.monthyear = nDate;
    else if (sid === '2')
        $scope.monthyear = $cookies.get("monthyear");

    $('#txtDate').datepicker({
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,

        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            var currmonth = $('#txtDate').val();
            $scope.monthyear = currmonth;
        },

        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('option', 'minDate', new Date(2017, 6, 1));
                var d = new Date();
                var n = d.getMonth();
                if (n != 0)
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear(), d.getMonth() - 1, 1));
                else
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear() - 1, 11, 1));
            }
        }
    });
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'Zone', field: 'Zone',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseCircle(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          {
              displayName: 'No.of Feeders', field: 'FeederNos',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          { displayName: 'Total Energy Drawl', field: 'TotalUnits' },
          { displayName: 'Total Units Billed', field: 'T1' },
          { displayName: '% T&D Loss', field: 'T2' },
          { displayName: 'Billled Amount', field: 'BA' },
          { displayName: 'Collected Amount', field: 'CA' },
          { displayName: '% AT&C Loss', field: 'ATCloss' }

        ]
    };
    $scope.getMonthZone = function (smonth) {
        var userid = 'admin';
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/FeederwiseEnergyZone/' + userid + "/" + smonth).then(function (FEZone) {
            $scope.gridOptions.data = FEZone.data;
            if (FEZone.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
    };
    $scope.getMonthZone($scope.monthyear);
    $scope.GetFeederwiseCircle = function (field) {
        var zone = field.Zone;
        $cookies.put("zone", zone);
        var zdate = $scope.monthyear;
        $location.path('/FeederwiseCircle/' + zone + '/' + zdate);
    };
    $scope.GetFeederwiseHistory = function (field) {

        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("FeederwiseEnergyHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var zone = field.Zone;
        $cookies.put("zone", zone);
        var zdate = $scope.monthyear
        $popup.szone = zone
        $popup.sdate = zdate;
        $popup.parm = "1";
    };
    $scope.back = function () {
        $location.path('/reports');
    }
})
.controller('FeederwiseCircleController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vdate = $routeParams.cdate;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $('#txtDate').datepicker({
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,

        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            var currmonth = $('#txtDate').val();
            $scope.monthyear = currmonth;
        },

        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('option', 'minDate', new Date(2017, 6, 1));
                var d = new Date();
                var n = d.getMonth();
                if (n != 0)
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear(), d.getMonth() - 1, 1));
                else
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear() - 1, 11, 1));
            }
        }
    });
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'Circle', field: 'Circle',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseDivision(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          {
              displayName: 'No.of Feeders', field: 'FeederNos',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          { displayName: 'Total Energy Drawl', field: 'TotalUnits' },
          { displayName: 'Total Units Billed', field: 'T1' },
          { displayName: '% T&D Loss', field: 'T2' },
          { displayName: 'Billled Amount', field: 'BA' },
          { displayName: 'Collected Amount', field: 'CA' },
          { displayName: '% AT&C Loss', field: 'ATCloss' }

        ]
    };
    $scope.getMonthCircle = function (smonth) {
        var userid = 'admin';
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/FeederwiseEnergyCircle/' + userid + "/" + vzone + "/" + smonth).then(function (FECircle) {
            $scope.gridOptions.data = FECircle.data;
            if (FECircle.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
    };
    $scope.getMonthCircle($scope.monthyear);
    $cookies.put("monthyear", $scope.monthyear);
    $scope.GetFeederwiseDivision = function (field) {
        var zone = $cookies.get("zone");
        var circle = field.Circle;
        $cookies.put("circle", circle);
        var zdate = $scope.monthyear
        $location.path('/FeederwiseDivision/' + zone + '/' + circle + '/' + zdate);
    };
    $scope.GetFeederwiseHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("FeederwiseEnergyHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var circle = field.Circle;
        var zdate = $scope.monthyear
        $popup.szone = $cookies.get("zone");
        $cookies.put("circle", circle);
        $popup.scircle = circle;
        $popup.sdate = zdate;
        $popup.parm = "2";
    };
    $scope.back = function () {
        $location.path('/FeederwiseZone/2');
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})
.controller('FeederwiseDivisionController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdate = $routeParams.cdate;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $('#txtDate').datepicker({
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,

        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            var currmonth = $('#txtDate').val();
            $scope.monthyear = currmonth;
        },

        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('option', 'minDate', new Date(2017, 6, 1));
                var d = new Date();
                var n = d.getMonth();
                if (n != 0)
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear(), d.getMonth() - 1, 1));
                else
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear() - 1, 11, 1));
            }
        }
    });
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'Division', field: 'Division',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseSubDivision(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          {
              displayName: 'No.of Feeders', field: 'FeederNos',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          { displayName: 'Total Energy Drawl', field: 'TotalUnits' },
          { displayName: 'Total Units Billed', field: 'T1' },
          { displayName: '% T&D Loss', field: 'T2' },
          { displayName: 'Billled Amount', field: 'BA' },
          { displayName: 'Collected Amount', field: 'CA' },
          { displayName: '% AT&C Loss', field: 'ATCloss' }

        ]
    };
    $scope.getMonthDivision = function (smonth) {
        var userid = 'admin';
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/FeederwiseEnergyDivision/' + userid + "/" + vzone + "/" + vcircle + "/" + smonth).then(function (FEDivision) {
            $scope.gridOptions.data = FEDivision.data;
            if (FEDivision.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
    };
    $scope.getMonthDivision($scope.monthyear);
    $cookies.put("monthyear", $scope.monthyear);
    $scope.GetFeederwiseSubDivision = function (field) {
        var zone = $cookies.get("zone");
        var circle = $cookies.get("circle");
        var division = field.Division
        $cookies.put("division", division);
        var zdate = $scope.monthyear
        $location.path('/FeederwiseSubDivision/' + zone + '/' + circle + '/' + division + '/' + zdate);
    };
    $scope.GetFeederwiseHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("FeederwiseEnergyHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var division = field.Division;
        var zdate = $scope.monthyear
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = division;
        $cookies.put("division", division);
        $popup.sdate = zdate;
        $popup.parm = "3";
    };
    $scope.back = function () {
        $location.path('/FeederwiseCircle/' + $cookies.get("zone") + '/' + $cookies.get("monthyear"));
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})
.controller('FeederwiseSubDivisionController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdivision = $routeParams.division;
    var vdate = $routeParams.cdate;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $scope.lblDivision = vdivision;
    $('#txtDate').datepicker({
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,

        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            var currmonth = $('#txtDate').val();
            $scope.monthyear = currmonth;
        },

        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('option', 'minDate', new Date(2017, 6, 1));
                var d = new Date();
                var n = d.getMonth();
                if (n != 0)
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear(), d.getMonth() - 1, 1));
                else
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear() - 1, 11, 1));
            }
        }
    });
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'SubDivision', field: 'SubDivision',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseSubDivision(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          {
              displayName: 'No.of Feeders', field: 'FeederNos',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          { displayName: 'Total Energy Drawl', field: 'TotalUnits' },
          { displayName: 'Total Units Billed', field: 'T1' },
          { displayName: '% T&D Loss', field: 'T2' },
          { displayName: 'Billled Amount', field: 'BA' },
          { displayName: 'Collected Amount', field: 'CA' },
          { displayName: '% AT&C Loss', field: 'ATCloss' }

        ]
    };
    $scope.getMonthSubDivision = function (smonth) {
        var userid = 'admin';
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/FeederwiseEnergySubDivision/' + userid + "/" + vzone + "/" + vcircle + "/" + vdivision.replace('&', '$') + "/" + smonth).then(function (FESubDivision) {
            $scope.gridOptions.data = FESubDivision.data;
            if (FESubDivision.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
    };
    $scope.getMonthSubDivision($scope.monthyear);
    $cookies.put("monthyear", $scope.monthyear);
    $scope.GetFeederwiseSubDivision = function (field) {
        var zone = $cookies.get("zone");
        var circle = $cookies.get("circle");
        var division = $cookies.get("division");
        var subdivision = field.SubDivision;
        $cookies.put("subdivision", subdivision);
        var zdate = $scope.monthyear
        $location.path('/FeederwiseSection/' + zone + '/' + circle + '/' + division + '/' + subdivision + '/' + zdate);
    };
    $scope.GetFeederwiseHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("FeederwiseEnergyHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var subdivision = field.SubDivision;
        $cookies.put("subdivision", subdivision);
        var zdate = $scope.monthyear
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = $cookies.get("division");
        $popup.ssubdivision = subdivision;
        $popup.sdate = zdate;
        $popup.parm = "4";
    };
    $scope.back = function () {
        $location.path('/FeederwiseDivision/' + $cookies.get("zone") + '/' + $cookies.get("circle") + '/' + $cookies.get("monthyear"));
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})
.controller('FeederwiseSectionController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdivision = $routeParams.division;
    var vsubdivision = $routeParams.subdivision;
    var vdate = $routeParams.cdate;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $scope.lblDivision = vdivision;
    $scope.lblSubDivision = vsubdivision;
    $('#txtDate').datepicker({
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,

        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            var currmonth = $('#txtDate').val();
            $scope.monthyear = currmonth;
        },

        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('option', 'minDate', new Date(2017, 6, 1));
                var d = new Date();
                var n = d.getMonth();
                if (n != 0)
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear(), d.getMonth() - 1, 1));
                else
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear() - 1, 11, 1));
            }
        }
    });
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              displayName: 'Junior Engineer', field: 'Section',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseJen(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          {
              displayName: 'No.of Feeders', field: 'FeederNos',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          { displayName: 'Total Energy Drawl', field: 'TotalUnits' },
          { displayName: 'Total Units Billed', field: 'T1' },
          { displayName: '% T&D Loss', field: 'T2' },
          { displayName: 'Billled Amount', field: 'BA' },
          { displayName: 'Collected Amount', field: 'CA' },
          { displayName: '% AT&C Loss', field: 'ATCloss' }

        ]
    };
    $scope.getMonthJen = function (smonth) {
        var userid = 'admin';
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/FeederwiseEnergyJen/' + userid + "/" + vzone + "/" + vcircle + "/" + vdivision.replace('&', '$') + "/" + vsubdivision.replace('&', '$') + "/" + smonth).then(function (FEJen) {
            $scope.gridOptions.data = FEJen.data;
            if (FEJen.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
    };
    $scope.getMonthJen($scope.monthyear);
    $cookies.put("monthyear", $scope.monthyear);
    $scope.GetFeederwiseJen = function (field) {
        var zone = $cookies.get("zone");
        var circle = $cookies.get("circle");
        var division = $cookies.get("division");
        var subdivision = $cookies.get("subdivision");
        var jen = field.Section;
        $cookies.put("jen", jen);
        var zdate = $scope.monthyear
        $location.path('/FeederwiseSubstation/' + zone + '/' + circle + '/' + division + '/' + subdivision + '/' + jen + '/' + zdate);
    };
    $scope.GetFeederwiseHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("FeederwiseEnergyHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var jen = field.Section;
        $cookies.put("jen", jen);
        var zdate = $scope.monthyear
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = $cookies.get("division");
        $popup.ssubdivision = $cookies.get("subdivision");
        $popup.sjen = jen;
        $popup.sdate = zdate;
        $popup.parm = "5";
    };
    $scope.back = function () {
        $location.path('/FeederwiseSubDivision/' + $cookies.get("zone") + '/' + $cookies.get("circle") + '/' + $cookies.get("division") + '/' + $cookies.get("monthyear"));
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }

})
.controller('FeederwiseSubstationController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdivision = $routeParams.division;
    var vsubdivision = $routeParams.subdivision;
    var vjen = $routeParams.jen;
    var vdate = $routeParams.cdate;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $scope.lblDivision = vdivision;
    $scope.lblSubDivision = vsubdivision;
    $scope.lblJen = vjen;
    $('#txtDate').datepicker({
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,

        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            var currmonth = $('#txtDate').val();
            $scope.monthyear = currmonth;
        },

        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('option', 'minDate', new Date(2017, 6, 1));
                var d = new Date();
                var n = d.getMonth();
                if (n != 0)
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear(), d.getMonth() - 1, 1));
                else
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear() - 1, 11, 1));
            }
        }
    });
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              displayName: 'Substation', field: 'Substation',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          {
              displayName: 'No.of Feeders', field: 'FeederNos',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetFeederwiseHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          { displayName: 'Total Energy Drawl', field: 'TotalUnits' },
          { displayName: 'Total Units Billed', field: 'T1' },
          { displayName: '% T&D Loss', field: 'T2' },
          { displayName: 'Billled Amount', field: 'BA' },
          { displayName: 'Collected Amount', field: 'CA' },
          { displayName: '% AT&C Loss', field: 'ATCloss' }

        ]
    };
    $scope.getMonthSubstation = function (smonth) {
        var userid = 'admin';
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/FeederwiseEnergySubstation/' + userid + "/" + vzone + "/" + vcircle + "/" + vdivision.replace('&', '$') + "/" + vsubdivision.replace('&', '$') + "/" + vjen.replace('&', '$') + "/" + smonth).then(function (FEJen) {
            $scope.gridOptions.data = FEJen.data;
            if (FEJen.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
    };
    $scope.getMonthSubstation($scope.monthyear);
    $cookies.put("monthyear", $scope.monthyear);
    $scope.GetFeederwiseHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("FeederwiseEnergyHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var substation = field.Substation;
        var zdate = $scope.monthyear
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = $cookies.get("division");
        $popup.ssubdivision = $cookies.get("subdivision");
        $popup.sjen = $cookies.get("jen");
        $popup.ssubstation = substation;
        $popup.sdate = zdate;
        $popup.parm = "6";
    };
    $scope.back = function () {
        $location.path('/FeederwiseSection/' + $cookies.get("zone") + '/' + $cookies.get("circle") + '/' + $cookies.get("division") + '/' + $cookies.get("subdivision") + '/' + $cookies.get("monthyear"));
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})

.controller('TrippingZoneController', function ($routeParams, $scope, $location, $filter, $http, $cookies, $window) {
    var d = new Date();
    var newMonth = d.getMonth() - 1;
    d.setDate(1);
    d.setMonth(newMonth);
    var nDate = $filter('date')(d, 'MMMM yyyy')
    var sid = $routeParams.id
    if (sid === 'R') {
        $scope.title = "Rural Area";
        $scope.monthyear = nDate;
        $cookies.put("type", sid);
    }
    else if (sid === 'U') {
        $scope.title = "Urban Area";
        $scope.monthyear = nDate;
        $cookies.put("type", sid);
    }
    else if (sid === 'I') {
        $scope.title = "Industrial Area";
        $scope.monthyear = nDate;
        $cookies.put("type", sid);
    }
    else if (sid === 'W') {
        $scope.title = "Water Works";
        $scope.monthyear = nDate;
        $cookies.put("type", sid);
    }
    //else if (sid === 'BR') {
    //    $scope.title = "Rural Area";
    //    $scope.monthyear = $cookies.get("monthyear");
    //    $cookies.put("type", "R");
    //    alert($cookies.get("type"));
    //}
    $('#txtDate').datepicker({
        showOn: 'button',
        buttonImage: 'images/cal.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'MM yy',
        endDate: '+0d',
        autoclose: true,

        onClose: function () {
            var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
            var currmonth = $('#txtDate').val();
            $scope.monthyear = currmonth;
        },

        beforeShow: function () {
            if ((selDate = $(this).val()).length > 0) {
                iYear = selDate.substring(selDate.length - 4, selDate.length);
                iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                $(this).datepicker('option', 'minDate', new Date(2017, 6, 1));
                var d = new Date();
                var n = d.getMonth();
                if (n != 0)
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear(), d.getMonth() - 1, 1));
                else
                    $(this).datepicker('option', 'maxDate', new Date(d.getFullYear() - 1, 11, 1));
            }
        }
    });
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'Zone', field: 'Zone',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingCircle(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          {
              displayName: 'No.of Feeders', field: 'FeederNos',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
          { displayName: 'Total No.of Trippings on Feeders', field: 'TotalTrippings' },
          { displayName: 'No.of Feeders Trippings < 3 Month', field: 'Lessthan3Trippings' },
          { displayName: 'Average monthly Trippings on the Feeders', field: 'AverageTrippings' }
        ]
    };
    $scope.getMonthZone = function (smonth) {
        debugger;
        var userid = 'admin';
        $cookies.put("monthyear", smonth);
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/TrippingsZone/' + userid + "/" + smonth + "/" + sid).then(function (TPZone) {
            $scope.gridOptions.data = TPZone.data;
            if (TPZone.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
    };
    $scope.getMonthZone($scope.monthyear);
    $scope.GetTrippingCircle = function (field) {
        var zone = field.Zone;
        $cookies.put("zone", zone);
        var zdate = $cookies.get("monthyear");
        var ztype = $cookies.get("type");
        $location.path('/TrippingCircle/' + zone + '/' + zdate + '/' + ztype);
    };
    $scope.GetTrippingHistory = function (field) {

        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("TrippingHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var zone = field.Zone;
        $cookies.put("zone", zone);
        var zdate = $scope.monthyear
        $popup.szone = zone
        $popup.sdate = zdate;
        $popup.stype = sid;
        $popup.parm = "1";
    };
    $scope.back = function () {
        $location.path('/reports');
    }
})
.controller('TrippingCircleController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vdate = $routeParams.cdate;
    var vtype = $routeParams.ctype;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'Circle', field: 'Circle',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingDivision(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
           {
               displayName: 'No.of Feeders', field: 'FeederNos',
               cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingHistory(row.entity)" > {{COL_FIELD}}</a> ',
               enableColumnMenu: false
           },
          { displayName: 'Total No.of Trippings on Feeders', field: 'TotalTrippings' },
          { displayName: 'No.of Feeders Trippings < 3 Month', field: 'Lessthan3Trippings' },
          { displayName: 'Average monthly Trippings on the Feeders', field: 'AverageTrippings' }
        ]
    };
    
        var userid = 'admin';
        $scope.loading = true;
        $scope.noData = true;
        $http.get('http://localhost:55130/Service1.svc/TrippingsCircle/' + userid + "/" + vzone + "/" + vdate + "/" + vtype).then(function (TPCircle) {
            $scope.gridOptions.data = TPCircle.data;
            if (TPCircle.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });
        $scope.GetTrippingDivision = function (field) {
        var zone = $cookies.get("zone");
        var circle = field.Circle;
        $cookies.put("circle", circle);
        var zdate = $cookies.get("monthyear");
        var ztype = $cookies.get("type");
        $location.path('/TrippingDivision/' + zone + '/' + circle + '/' + zdate + '/' + ztype);
    };
    $scope.GetTrippingHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("TrippingHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var circle = field.Circle;
        var zdate = $cookies.get("monthyear");
        $popup.szone = $cookies.get("zone");
        $cookies.put("circle", circle);
        $popup.scircle = circle;
        $popup.sdate = zdate;
        $popup.stype = $cookies.get("type");
        $popup.parm = "2";
    };
    $scope.back = function () {
        $location.path('/TrippingZone/' + $cookies.get("type"));
    };
    $scope.home = function () {
        $location.path('/TrippingZone/' + $cookies.get("type"));
    }
})
.controller('TrippingDivisionController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdate = $routeParams.cdate;
    var vtype = $routeParams.ctype;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'Division', field: 'Division',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingSubDivision(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
           {
               displayName: 'No.of Feeders', field: 'FeederNos',
               cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingHistory(row.entity)" > {{COL_FIELD}}</a> ',
               enableColumnMenu: false
           },
          { displayName: 'Total No.of Trippings on Feeders', field: 'TotalTrippings' },
          { displayName: 'No.of Feeders Trippings < 3 Month', field: 'Lessthan3Trippings' },
          { displayName: 'Average monthly Trippings on the Feeders', field: 'AverageTrippings' }
        ]
    };

    var userid = 'admin';
    $scope.loading = true;
    $scope.noData = true;
    $http.get('http://localhost:55130/Service1.svc/TrippingsDivision/' + userid + "/" + vzone + "/" + vcircle + "/" + vdate + "/" + vtype).then(function (TPDivision) {
        $scope.gridOptions.data = TPDivision.data;
        if (TPDivision.data.length === 0) {
            $scope.noData = false;
        }
        $scope.loading = false;
    });
    $scope.GetTrippingSubDivision = function (field) {
        var zone = $cookies.get("zone");
        var circle = $cookies.get("circle");
        var division = field.Division
        $cookies.put("division", division);
        var zdate = $cookies.get("monthyear");
        var ztype = $cookies.get("type");
        $location.path('/TrippingSubDivision/' + zone + '/' + circle + '/' + division  + "/" + zdate + '/' + ztype);
    };
    $scope.GetTrippingHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("TrippingHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var division = field.Division;
        var zdate = $cookies.get("monthyear");
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = division;
        $cookies.put("division", division);
        $popup.sdate = zdate;
        $popup.stype = $cookies.get("type");
        $popup.parm = "3";
    };
    $scope.back = function () {
        $location.path('/FeederwiseZone/2');
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})
.controller('TrippingSubDivisionController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdivision = $routeParams.division;
    var vdate = $routeParams.cdate;
    var vtype = $routeParams.ctype;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $scope.lblDivision = vdivision;
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              name: 'SubDivision', field: 'SubDivision',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingSubDivision(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
           {
               displayName: 'No.of Feeders', field: 'FeederNos',
               cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingHistory(row.entity)" > {{COL_FIELD}}</a> ',
               enableColumnMenu: false
           },
          { displayName: 'Total No.of Trippings on Feeders', field: 'TotalTrippings' },
          { displayName: 'No.of Feeders Trippings < 3 Month', field: 'Lessthan3Trippings' },
          { displayName: 'Average monthly Trippings on the Feeders', field: 'AverageTrippings' }
        ]
    };

    var userid = 'admin';
    $scope.loading = true;
    $scope.noData = true;
    $http.get('http://localhost:55130/Service1.svc/TrippingsSubdivision/' + userid + "/" + vzone + "/" + vcircle + "/" + vdivision.replace('&', '$') + "/" + vdate + "/" + vtype).then(function (TPSubDivision) {
        $scope.gridOptions.data = TPSubDivision.data;
        if (TPSubDivision.data.length === 0) {
            $scope.noData = false;
        }
        $scope.loading = false;
    });
    $scope.GetTrippingSubDivision = function (field) {
        var zone = $cookies.get("zone");
        var circle = $cookies.get("circle");
        var division = $cookies.get("division");
        var subdivision = field.SubDivision;
        $cookies.put("subdivision", subdivision);
        var zdate = $cookies.get("monthyear");
        var ztype = $cookies.get("type");
        $location.path('/TrippingSection/' + zone + '/' + circle + '/' + division + '/' + subdivision + "/" + zdate + '/' + ztype);
    };
    $scope.GetTrippingHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("TrippingHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        
        var zdate = $cookies.get("monthyear");
        var subdivision = field.SubDivision;
        $cookies.put("subdivision", subdivision);
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = $cookies.get("division");
        $popup.ssubdivision = subdivision;
        $popup.sdate = zdate;
        $popup.stype = $cookies.get("type");
        $popup.parm = "4";

        
    };
    $scope.back = function () {
        $location.path('/FeederwiseZone/2');
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})
.controller('TrippingSectionController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdivision = $routeParams.division;
    var vsubdivision = $routeParams.subdivision;
    var vdate = $routeParams.cdate;
    var vtype = $routeParams.ctype;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $scope.lblDivision = vdivision;
    $scope.lblSubDivision = vsubdivision;
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              displayName: 'Junior Engineer', field: 'Section',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingJen(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
           {
               displayName: 'No.of Feeders', field: 'FeederNos',
               cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingHistory(row.entity)" > {{COL_FIELD}}</a> ',
               enableColumnMenu: false
           },
          { displayName: 'Total No.of Trippings on Feeders', field: 'TotalTrippings' },
          { displayName: 'No.of Feeders Trippings < 3 Month', field: 'Lessthan3Trippings' },
          { displayName: 'Average monthly Trippings on the Feeders', field: 'AverageTrippings' }
        ]
    };

    var userid = 'admin';
    $scope.loading = true;
    $scope.noData = true;
    $http.get('http://localhost:55130/Service1.svc/TrippingsJen/' + userid + "/" + vzone + "/" + vcircle + "/" + vdivision.replace('&', '$') + "/" + vsubdivision.replace('&', '$') + "/" + vdate + "/" + vtype).then(function (TPJen) {
        $scope.gridOptions.data = TPJen.data;
        if (TPJen.data.length === 0) {
            $scope.noData = false;
        }
        $scope.loading = false;
    });
    $scope.GetTrippingJen = function (field) {
        var zone = $cookies.get("zone");
        var circle = $cookies.get("circle");
        var division = $cookies.get("division");
        var subdivision = $cookies.get("subdivision");
        var jen = field.Section;
        $cookies.put("JEN", jen)
        var zdate = $cookies.get("monthyear");
        var ztype = $cookies.get("type");
        $location.path('/TrippingSubstation/' + zone + '/' + circle + '/' + division + '/' + subdivision + '/' + jen + "/" + zdate + '/' + ztype);
    };
    $scope.GetTrippingHistory = function (field) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("TrippingHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var zdate = $cookies.get("monthyear");
        var jen = field.Section;
        $cookies.put("JEN", jen);
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = $cookies.get("division");
        $popup.ssubdivision = $cookies.get("subdivision");
        $popup.sjen = jen;
        $popup.sdate = zdate;
        $popup.stype = $cookies.get("type");
        $popup.parm = "5";
    };
    $scope.back = function () {
        $location.path('/FeederwiseZone/2');
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})
.controller('TrippingSubstationController', function ($routeParams, $scope, $location, $http, $cookies, $window) {
    var vzone = $routeParams.zone;
    var vcircle = $routeParams.circle;
    var vdivision = $routeParams.division;
    var vsubdivision = $routeParams.subdivision;
    var vjen = $routeParams.jen;
    var vdate = $routeParams.cdate;
    var vtype = $routeParams.ctype;
    $scope.monthyear = vdate;
    $scope.lblZone = vzone;
    $scope.lblCircle = vcircle;
    $scope.lblDivision = vdivision;
    $scope.lblSubDivision = vsubdivision;
    $scope.lblJen = vjen;
    $scope.gridOptions = {
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterCsvFilename: 'myFile.csv',
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterExcelFilename: 'myFile.xlsx',
        exporterExcelSheetName: 'Sheet1',
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        enableSorting: false,
        enableFiltering: false,
        columnDefs: [
          { name: 'S.No', field: 'SNo' },
          {
              displayName: 'Substation', field: 'Substation',
              cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingHistory(row.entity)" > {{COL_FIELD}}</a> ',
              enableColumnMenu: false
          },
           {
               displayName: 'No.of Feeders', field: 'FeederNos',
               cellTemplate: '<a class="btn-small" ng-click="grid.appScope.GetTrippingHistory(row.entity)" > {{COL_FIELD}}</a> ',
               enableColumnMenu: false
           },
          { displayName: 'Total No.of Trippings on Feeders', field: 'TotalTrippings' },
          { displayName: 'No.of Feeders Trippings < 3 Month', field: 'Lessthan3Trippings' },
          { displayName: 'Average monthly Trippings on the Feeders', field: 'AverageTrippings' }
        ]
    };

    var userid = 'admin';
    $scope.loading = true;
    $scope.noData = true;
    $http.get('http://localhost:55130/Service1.svc/TrippingsSubstation/' + userid + "/" + vzone + "/" + vcircle + "/" + vdivision.replace('&', '$') + "/" + vsubdivision.replace('&', '$') + "/" + vjen.replace('&','$') + "/" + vdate + "/" + vtype).then(function (TPSubstation) {
        $scope.gridOptions.data = TPSubstation.data;
        if (TPSubstation.data.length === 0) {
            $scope.noData = false;
        }
        $scope.loading = false;
    });
    $scope.GetTrippingHistory = function (field) {
        debugger;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        var $popup = $window.open("TrippingHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

        var zdate = $cookies.get("monthyear");
        var substation = field.Substation;
        $popup.szone = $cookies.get("zone");
        $popup.scircle = $cookies.get("circle");
        $popup.sdivision = $cookies.get("division");
        $popup.ssubdivision = $cookies.get("subdivision");
        $popup.sjen = $cookies.get("JEN");
        $popup.ssubstation = substation
        $popup.sdate = zdate;
        $popup.stype = $cookies.get("type");
        $popup.parm = "6";
    };
    $scope.back = function () {
        $location.path('/FeederwiseZone/2');
    };
    $scope.home = function () {
        $location.path('/FeederwiseZone/2');
    }
})




