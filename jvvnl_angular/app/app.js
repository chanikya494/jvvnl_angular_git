﻿(function () {
    'use strict';

    angular.module('myApp', ['ngRoute', 'ngCookies', 'ngAnimate', 'ngMaterial', 'ngAria', 'ngSanitize', 'ui.bootstrap', 'ngTouch', 'ui.grid','ui.grid.expandable', 'ui.grid.pagination', 'ui.grid.exporter'])
        .config(config)
    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];

    function config($routeProvider, $locationProvider, $httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix('');
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $routeProvider
            .when('/home/:id', {
                url:'home',
                controller: 'homeController',
                templateUrl: 'Home.html'
            })
            .when('/', {
                url: 'login',
                controller: 'loginController',
                templateUrl: 'login.html'
            })
             .when('/reports', {
                 url: 'reports',
                 controller: 'reportController',
                 templateUrl: 'reports.html'
             })
             .when('/rptQuality/:param', {
                 url: 'Quality',
                 controller: 'rptQualityController',
                 templateUrl: 'frmrptsQualityPowerSupply.html'
             })
            .when('/FeederwiseZone/:id', {
                url: 'FeederwiseZone',
                controller: 'FeederwiseZoneController',
                templateUrl: 'FeederwiseEnergyReport.html'
            })
            .when('/FeederwiseCircle/:zone/:cdate', {
                url: 'FeederwiseCircle',
                controller: 'FeederwiseCircleController',
                templateUrl: 'FeederwiseCircle.html'
            })
             .when('/FeederwiseDivision/:zone/:circle/:cdate', {
                 url: 'FeederwiseDivision',
                 controller: 'FeederwiseDivisionController',
                 templateUrl: 'FeederwiseDivision.html'
             })
             .when('/FeederwiseSubDivision/:zone/:circle/:division/:cdate', {
                 url: 'FeederwiseSubDivision',
                 controller: 'FeederwiseSubDivisionController',
                 templateUrl: 'FeederwiseSubDivision.html'
             })
             .when('/FeederwiseSection/:zone/:circle/:division/:subdivision/:cdate', {
                 url: 'FeederwiseSection',
                 controller: 'FeederwiseSectionController',
                 templateUrl: 'FeederwiseSection.html'
             })
            .when('/FeederwiseSubstation/:zone/:circle/:division/:subdivision/:jen/:cdate', {
                url: 'FeederwiseSubstation',
                controller: 'FeederwiseSubstationController',
                templateUrl: 'FeederwiseSubstation.html'
            })
             .when('/TrippingZone/:id', {
                 url: 'TrippingZone',
                 controller: 'TrippingZoneController',
                 templateUrl: 'TrippingReport.html'
             })
            .when('/TrippingCircle/:zone/:cdate/:ctype', {
                url: 'TrippingCircle',
                controller: 'TrippingCircleController',
                templateUrl: 'TrippingCircle.html'
            })
             .when('/TrippingDivision/:zone/:circle/:cdate/:ctype', {
                 url: 'TrippingDivision',
                 controller: 'TrippingDivisionController',
                 templateUrl: 'TrippingDivision.html'
             })
             .when('/TrippingSubDivision/:zone/:circle/:division/:cdate/:ctype', {
                 url: 'TrippingSubDivision',
                 controller: 'TrippingSubDivisionController',
                 templateUrl: 'TrippingSubDivision.html'
             })
             .when('/TrippingSection/:zone/:circle/:division/:subdivision/:cdate/:ctype', {
                 url: 'TrippingSection',
                 controller: 'TrippingSectionController',
                 templateUrl: 'TrippingSection.html'
             })
            .when('/TrippingSubstation/:zone/:circle/:division/:subdivision/:jen/:cdate/:ctype', {
                url: 'TrippingSubstation',
                controller: 'TrippingSubstationController',
                templateUrl: 'TrippingSubstation.html'
            })
            .otherwise({ redirectTo: '/' });
    }
})();