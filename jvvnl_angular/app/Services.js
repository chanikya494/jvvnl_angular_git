﻿angular.module('myApp')
.service('zoneService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/ZoneData/admin";
    this.drpzone = function () {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase).then(function (zonedata) {
                resolve(zonedata);
            });
        })
    }
})

.service('circleService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/CircleData/";
    this.drpcircle = function (zoneval) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + zoneval + "/admin").then(function (circledata) {
                resolve(circledata);
            });
        })
    }
})

.service('mlaService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/MlaData/";
    this.drpmla = function (zoneval, circleval) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + zoneval + "/" + circleval + "/admin").then(function (mladata) {
                resolve(mladata);
            });
        })
    }
})

.service('divService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/DivisionData/";
    this.drpdiv = function (zoneval, circleval, mlaval) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + zoneval + "/" + circleval + "/" + mlaval + "/admin").then(function (divdata) {
                resolve(divdata);
            });
        })
    }
})

.service('subdivService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/SubDivisionData/";
    this.drpsubdiv = function (zoneval, circleval, mlaval, divval) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + zoneval + "/" + circleval + "/" + mlaval + "/" + divval + "/admin").then(function (subdivdata) {
                resolve(subdivdata);
            });
        })
    }
})

.service('jenService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/JenData/";
    this.drpjen = function (zoneval, circleval, mlaval, divval, subdivval) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + zoneval + "/" + circleval + "/" + mlaval + "/" + divval + "/" + subdivval + "/admin").then(function (jendata) {
                resolve(jendata);
            });
        })
    }
})

.service('pieService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/PieData/";
    this.pievalues = function (zoneval, circleval, mlaval, divval, subdivval, jenval) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin/" + zoneval + "/" + circleval + "/" + mlaval + "/" + divval + "/" + subdivval + "/" + jenval).then(function (piedata) {
                resolve(piedata);
            });
        })
    }
})

.service('subtationService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/SubstationData/";
    this.substations = function (zoneval, circleval, mlaval, divval, subdivval, jenval) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin/" + zoneval + "/" + circleval + "/" + mlaval + "/" + divval + "/" + subdivval + "/" + jenval).then(function (subdata) {
                resolve(subdata);
            });
        })
    }
})

.service('phaseService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/PhaseData/";
    this.phases = function (zoneval, circleval, mlaval, divval, subdivval, jenval, estatus, pdate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin/" + zoneval + "/" + circleval + "/" + mlaval + "/" + divval + "/" + subdivval + "/" + jenval + "/" + estatus + "/" + pdate).then(function (phasedata) {
                resolve(phasedata);
            });
        })
    }
})

.service('BlocakLoad', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/BlocakLoad/";
    this.phases = function (meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + meterno).then(function (loadData) {
                resolve(loadData);
            });
        })
    }
})

.service('halfPeakLoadService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/HalfHourPeakLoad/";
    this.halfPeakLoad = function (zone, circle, mla, division, subdivision, jen, startdate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + startdate).then(function (peakloaddata) {
                resolve(peakloaddata);
            })
        })
    }
})

.service('substationPeakLoadService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/DaywiseSubstationPeakLoad/";
    this.subPeak = function (zone, circle, mla, division, subdivision, jen, startdate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + startdate).then(function (subdata) {
                resolve(subdata);
            })
        })
    }
})

.service('pfAbstractService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/pfAbstract/";
    this.pfAbstract = function (zone, circle, mla, division, subdivision, jen, pfdate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin" + "/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + pfdate).then(function (pfdata) {
                resolve(pfdata);
            })
        })
    }
})

.service('overloadService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getOverlad/";
    this.overload = function (zone, circle, mla, division, subdivision, jen, oudate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin" + "/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + oudate).then(function (overdata) {
                resolve(overdata);
            })
        })
    }
})

.service('interruptionService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getfeederInterruption/";
    this.interruption = function (zone, circle, mla, division, subdivision, jen, itrdate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin" + "/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + itrdate).then(function (itrdata) {
                resolve(itrdata);
            })
        })
    }
})

.service('instantsService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getMeterInstants/";
    this.instants = function (zone, circle, mla, division, subdivision, jen) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin" + "/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen).then(function (instantsdata) {
                resolve(instantsdata);
            })
        })
    }
})

.service('notCommService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getFeederNotComm/";
    this.notComm = function (zone, circle, mla, division, subdivision, jen) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin" + "/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen).then(function (notcommdata) {
                resolve(notcommdata);
            })
        })
    }
})

.service('logParent', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getParent/";
    this.logParent = function (zone, circle, mla, division, subdivision, jen, logdate) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin" + "/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + logdate).then(function (parentdata) {
                resolve(parentdata);
            });
        })
    }
})

.service('logChild', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/getChild/";
    this.logChild = function (zone, circle, mla, division, subdivision, jen, logdate, meterno) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + "admin" + "/" + zone + "/" + circle + "/" + mla + "/" + division + "/" + subdivision + "/" + jen + "/" + logdate + "/" + meterno).then(function (childdata) {
                resolve(childdata);
            });
        })
    }
})

.service('loginService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/login/";
    this.login = function (userid, password) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + userid + "/" + password).then(function (logdata) {
                resolve(logdata);
            })
        })
    }
})

.service('areaService', function ($q, $http) {
    var serviceBase = "http://localhost:55130/Service1.svc/PhaseAreawiseData/";
    this.area = function (substation, feedername) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + substation + "/" + feedername).then(function (areadata) {
                resolve(areadata);
            });
        })
    }
})
.service('changePasswordService', function ($q, $http) {
    var serviceBase = "https://fdrms.visiontek.co.in:9004/feederdetails/Service1.svc/Rest/changeuserpwd/";
    this.changePassword = function (userid, oldPassword, newPassword) {
        return $q(function (resolve, reject) {
            return $http.get(serviceBase + userid + "/" + oldPassword + "/" + newPassword).then(function (status) {
                resolve(status);
            })
        })
    }
})


