﻿angular.module('myApp')
.factory('accessFac', function () {
    var obj = {}
    this.access = false;
    obj.getPermission = function () {    //set the permission to true
        this.access = true;
    }
    obj.checkPermission = function () {
        return this.access;             //returns the users permission level 
    }
    return obj;
})

.controller('masterController', function ($scope, $location, $window, changePasswordService, $rootScope) {

    function getBGPdata(searchValue) {
        console.log('--> ' + searchValue);
        alert(searchValue);
    };

    $scope.callDashboard = function () {
        $location.path('/home/' + angular.element(document.querySelector('#username'))[0].innerText);
    };

    $scope.callLogout = function () {
        $location.path('/');
        angular.element(document.querySelector('#username'))[0].innerText = "";
    };

    $scope.callGeoMap = function () {
        var userid = angular.element(document.querySelector('#username'))[0].innerText;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
        var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
        $window.open("https://fdrms.visiontek.co.in:9004/SubstationMap/geoMap.aspx?userid=" + userid, 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);
    }

    $scope.changePassword = function () {
        var userid = angular.element(document.querySelector('#username'))[0].innerText;
        var url = "frmChangePassword.html";

        var x = screen.width / 2 - 1250 / 2;
        var y = screen.height / 2 - 740 / 2;
        var $popup = $window.open(url, 'hai', "height=710,width=1250,status=yes,left=" + x + ",top=" + y);
        $popup.chaneUserid = userid;
        $popup.pswrd = $rootScope.pwd;
        $popup.uname = $rootScope.username;
    }
})

.controller('loginController', function ($scope, $location, loginService, $rootScope, $cookies) {
    $scope.style = {
        'background-image': 'url("http://localhost:56121/images/login.jpg")'
    }
    console.log($scope.style);
    $scope.formSubmit = function (user) {
        loginService.login(user.userid, user.password).then(function (data) {
            if (data.data[0].status === "true") {
                $rootScope.pwd = user.password;
                $rootScope.username = data.data[0].username;
                $cookies.put('username', data.data[0].username);
                $cookies.put('userid', data.data[0].userid);
                $location.path('/home/' + data.data[0].userid);
            }
            else {
                alert("Invalid Username and Password");
            }
        })
    }
})

.controller('homeController', function (accessFac, $filter, zoneService, circleService, mlaService, divService, subdivService, jenService, pieService, subtationService, phaseService, halfPeakLoadService, $scope, substationPeakLoadService, pfAbstractService, overloadService, interruptionService, instantsService, notCommService, areaService, uiGridConstants, $http, $window, $uibModal, $rootScope, $cookies, $routeParams, logParent, logChild) {
    var zoneval, circleval, mlaval, divval, subdivval, jenval, pdate, userid;
    $rootScope.data = "";

    $scope.showThreePhase = false;
    $scope.showTwoPhase = false;
    $scope.showSinglePhase = false;
    $scope.showPowerFailurePhase = false;
    $scope.showLogBook = false;
    $scope.showHalfPeakLoad = false;
    $scope.showSubfPeakLoad = false;
    $scope.showPF = false;
    $scope.showOverload = false;
    $scope.showInt = false;
    $scope.showInstants = false;
    $scope.shownotComm = false;

    $scope.today = new Date();

    userid = $routeParams.id;
    var username = $cookies.get('username');

    angular.element(document.querySelector('#username'))[0].innerText = username;

    var uid = userid.toLowerCase();
    var showcontent = uid.includes("mla");

    if (showcontent) {
        $scope.hidemla_alldata = true;
        $scope.hidemla_Date = true;
        $scope.hidemla_pie = true;
        $scope.hidemla_DropDown = true;
    }



    var yes = new Date();
    var dd = yes.getDate() - 1;
    var mm = yes.getMonth();
    var yy = yes.getFullYear();
    $scope.txtDate = new Date(yy, mm, dd);

    $scope.dateOptions = {
        formatDay: 'dd',
        formatMonth: 'MM',
        formatYear: 'yyyy',
        maxDate: new Date(yy, mm, dd),
        showWeeks: false
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.format = 'dd-MM-yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.popup1 = {
        opened: false
    };

    zoneService.drpzone().then(function (zonedata) {
        $scope.zones = zonedata.data;
        $scope.dzone = $scope.zones[0];
        zoneval = $scope.dzone.zone;
        circleService.drpcircle(zoneval).then(function (circledata) {
            $scope.circles = circledata.data;
            $scope.dcircle = $scope.circles[0];
            circleval = $scope.dcircle.circle;
            mlaService.drpmla(zoneval, circleval).then(function (mladata) {
                $scope.mlas = mladata.data;
                $scope.dmla = $scope.mlas[0];
                mlaval = $scope.dmla.mla;
                divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                    $scope.divs = divdata.data;
                    $scope.ddiv = $scope.divs[0];
                    divval = $scope.ddiv.div;
                    subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                        $scope.subdivs = subdivdata.data;
                        $scope.dsubdiv = $scope.subdivs[0];
                        subdivval = $scope.dsubdiv.subdiv;
                        jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                            $scope.jens = jendata.data;
                            $scope.djen = $scope.jens[0];
                            jenval = $scope.djen.jen;
                            pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                                GetPiechart(piedata.data[0]);
                                $scope.total = piedata.data[0].total;
                                $scope.threeph = piedata.data[0].threephase;
                                $scope.twoph = piedata.data[0].twophase;
                                $scope.singph = piedata.data[0].singlephase;
                                $scope.noph = piedata.data[0].nophase;
                                $scope.ncph = piedata.data[0].notcomm;
                                $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                            })

                            subtationService.substations(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (subdata) {
                                $scope.data = {
                                    photos: subdata.data,
                                    photos3p: partition(subdata.data, (subdata.data.length) / 5)
                                };
                            })
                        })
                    })
                })
            })
        });

        $scope.GetCircle = function () {
            zoneval = $scope.dzone.zone;
            circleService.drpcircle(zoneval).then(function (circledata) {
                $scope.circles = circledata.data;
                $scope.dcircle = $scope.circles[0];
                circleval = $scope.dcircle.circle;
                mlaService.drpmla(zoneval, circleval).then(function (mladata) {
                    $scope.mlas = mladata.data;
                    $scope.dmla = $scope.mlas[0];
                    mlaval = $scope.dmla.mla;
                    divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                        $scope.divs = divdata.data;
                        $scope.ddiv = $scope.divs[0];
                        divval = $scope.ddiv.div;
                        subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                            $scope.subdivs = subdivdata.data;
                            $scope.dsubdiv = $scope.subdivs[0];
                            subdivval = $scope.dsubdiv.subdiv;
                            jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                                $scope.jens = jendata.data;
                                $scope.djen = $scope.jens[0];
                                jenval = $scope.djen.jen;
                                pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                                    GetPiechart(piedata.data[0]);
                                    $scope.total = piedata.data[0].total;
                                    $scope.threeph = piedata.data[0].threephase;
                                    $scope.twoph = piedata.data[0].twophase;
                                    $scope.singph = piedata.data[0].singlephase;
                                    $scope.noph = piedata.data[0].nophase;
                                    $scope.ncph = piedata.data[0].notcomm;
                                    $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                                    $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                                })
                            })
                        })
                    })
                })
            })
        };

        $scope.GetMla = function () {

            zoneval = $scope.dzone.zone;
            circleval = $scope.dcircle.circle;
            mlaService.drpmla(zoneval, circleval).then(function (mladata) {
                $scope.map = {};

                //the following two lines added for showing ALL for MLA dropdown
                $scope.map['mla'] = "ALL";
                mladata.data.splice(0, 0, $scope.map);

                $scope.mlas = mladata.data;
                $scope.dmla = $scope.mlas[0];
                mlaval = $scope.dmla.mla;
                divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                    $scope.divs = divdata.data;
                    $scope.ddiv = $scope.divs[0];
                    divval = $scope.ddiv.div;
                    subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                        $scope.subdivs = subdivdata.data;
                        $scope.dsubdiv = $scope.subdivs[0];
                        subdivval = $scope.dsubdiv.subdiv;
                        jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                            $scope.jens = jendata.data;
                            $scope.djen = $scope.jens[0];
                            jenval = $scope.djen.jen;
                            pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                                GetPiechart(piedata.data[0]);
                                $scope.total = piedata.data[0].total;
                                $scope.threeph = piedata.data[0].threephase;
                                $scope.twoph = piedata.data[0].twophase;
                                $scope.singph = piedata.data[0].singlephase;
                                $scope.noph = piedata.data[0].nophase;
                                $scope.ncph = piedata.data[0].notcomm;
                                $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                                $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                            })
                        })
                    })
                })
            })
        };

        $scope.GetDiv = function () {
            zoneval = $scope.dzone.zone;
            circleval = $scope.dcircle.circle;
            mlaval = $scope.dmla.mla;
            divService.drpdiv(zoneval, circleval, mlaval).then(function (divdata) {
                $scope.divs = divdata.data;
                $scope.ddiv = $scope.divs[0];
                divval = $scope.ddiv.div;
                subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                    $scope.subdivs = subdivdata.data;
                    $scope.dsubdiv = $scope.subdivs[0];
                    subdivval = $scope.dsubdiv.subdiv;
                    jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                        $scope.jens = jendata.data;
                        $scope.djen = $scope.jens[0];
                        jenval = $scope.djen.jen;
                        pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                            GetPiechart(piedata.data[0]);
                            $scope.total = piedata.data[0].total;
                            $scope.threeph = piedata.data[0].threephase;
                            $scope.twoph = piedata.data[0].twophase;
                            $scope.singph = piedata.data[0].singlephase;
                            $scope.noph = piedata.data[0].nophase;
                            $scope.ncph = piedata.data[0].notcomm;
                            $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                            $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                        })
                    })
                })
            })
        };

        $scope.GetSubDiv = function () {
            zoneval = $scope.dzone.zone;
            circleval = $scope.dcircle.circle;
            mlaval = $scope.dmla.mla;
            divval = $scope.ddiv.div;
            subdivService.drpsubdiv(zoneval, circleval, mlaval, divval).then(function (subdivdata) {
                $scope.subdivs = subdivdata.data;
                $scope.dsubdiv = $scope.subdivs[0];
                subdivval = $scope.dsubdiv.subdiv;
                jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                    $scope.jens = jendata.data;
                    $scope.djen = $scope.jens[0];
                    jenval = $scope.djen.jen;
                    pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                        GetPiechart(piedata.data[0]);
                        $scope.total = piedata.data[0].total;
                        $scope.threeph = piedata.data[0].threephase;
                        $scope.twoph = piedata.data[0].twophase;
                        $scope.singph = piedata.data[0].singlephase;
                        $scope.noph = piedata.data[0].nophase;
                        $scope.ncph = piedata.data[0].notcomm;
                        $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                        $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                    })
                })
            })
        };

        $scope.GetJen = function () {
            zoneval = $scope.dzone.zone;
            circleval = $scope.dcircle.circle;
            mlaval = $scope.dmla.mla;
            divval = $scope.ddiv.div;
            subdivval = $scope.dsubdiv.subdiv;
            jenService.drpjen(zoneval, circleval, mlaval, divval, subdivval).then(function (jendata) {
                $scope.jens = jendata.data;
                $scope.djen = $scope.jens[0];
                jenval = $scope.djen.jen;
                pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                    GetPiechart(piedata.data[0]);
                    $scope.total = piedata.data[0].total;
                    $scope.threeph = piedata.data[0].threephase;
                    $scope.twoph = piedata.data[0].twophase;
                    $scope.singph = piedata.data[0].singlephase;
                    $scope.noph = piedata.data[0].nophase;
                    $scope.ncph = piedata.data[0].notcomm;
                    $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                    $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
                })
            })
        };

        $scope.GetPie = function () {
            zoneval = $scope.dzone.zone;
            circleval = $scope.dcircle.circle;
            mlaval = $scope.dmla.mla;
            divval = $scope.ddiv.div;
            subdivval = $scope.dsubdiv.subdiv;
            jenval = $scope.djen.jen;
            pieService.pievalues(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (piedata) {
                console.log(piedata.data);
                GetPiechart(piedata.data[0]);
                $scope.total = piedata.data[0].total;
                $scope.threeph = piedata.data[0].threephase;
                $scope.twoph = piedata.data[0].twophase;
                $scope.singph = piedata.data[0].singlephase;
                $scope.noph = piedata.data[0].nophase;
                $scope.ncph = piedata.data[0].notcomm;
                $scope.div_3ph = parseInt(piedata.data[0].threephase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_2ph = parseInt(piedata.data[0].twophase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_1ph = parseInt(piedata.data[0].singlephase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_pf = parseInt(piedata.data[0].nophase) * 100 / parseInt(piedata.data[0].total);
                $scope.div_notcomm = parseInt(piedata.data[0].notcomm) * 100 / parseInt(piedata.data[0].total);
            })
        }
    });

    function GetPiechart(pdata) {
        Highcharts.chart('feedercontainer', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 60,
                    beta: 0
                }
            },
            title: {
                text: '',
                style: {
                    font: '300 2.56rem Roboto',
                    color: '#ee9800'
                },
                align: 'left',
                y: 45
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.point.name + '</b>: ' + this.y;
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 55,
                    dataLabels: {
                        enabled: false
                    },
                    //animation: false,
                    point: {
                        events: {
                            click: function () {
                                var options = this.options;
                                //location.href = 'frmSinglePhase.aspx?parameter=' + options.name
                            }
                        }
                    }
                }
            },
            series: [{
                name: 'Browser share',
                size: '115%',
                data: [{
                    name: 'Three Phase Running',
                    y: parseInt(pdata.threephase)
                }, {
                    name: 'Two Phase Running',
                    y: parseInt(pdata.twophase)
                }, {
                    name: 'Single Phase Running',
                    y: parseInt(pdata.singlephase)
                }, {
                    name: 'Power Failure',
                    y: parseInt(pdata.nophase)
                }, {
                    name: 'Not Communicated',
                    y: parseInt(pdata.notcomm)
                }]
            }]
        });

    };

    function partition(input, size) {
        var newArr = [];
        for (var i = 0; i < input.length; i += size) {
            newArr.push(input.slice(i, i + size));
        }
        return newArr;
    };

    $scope.substationClicked = function (substn) {
        var url = " https://fdrms.visiontek.co.in:9004/SubstationMap/FeederMap.aspx?userid=" + userid + "&zone=" + zoneval + "&circle=" + circleval + "&mla=" + mlaval + "&div=" + divval + "&sdiv=" + subdivval + "&jen=" + jenval + "&substation=" + substn + "";
        var x = screen.width / 2 - 1250 / 2;
        var y = screen.height / 2 - 740 / 2;
        $window.open(url, 'hai', "height=710,width=1250,status=yes,left=" + x + ",top=" + y);
    };

    $scope.feederClicked = function (fdrname, substn) {
        var url = "  https://fdrms.visiontek.co.in/frmFeederMasterData.aspx?substation=" + substn + " &feedername=" + fdrname + "";
        var x = screen.width / 2 - 1250 / 2;
        var y = screen.height / 2 - 740 / 2;
        $window.open(url, 'hai', "height=710,width=1250,status=yes,left=" + x + ",top=" + y);
    }

    $scope.dateChange = function (urlDate) {
        debugger;
        //$scope.threePhaseClicked(urlDate);
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        $scope.ima = true;
    };

    $scope.threePhaseClicked = function (urlDate, status) {
        var estatus = status;
        $scope.ima = true;
        $scope.loading = true;
        $scope.noData = true;
        if (status === 'Three Phase Running') {
            $scope.showThreePhase = $scope.showThreePhase === false ? true : false;
        }
        else if (status === 'Two Phase Running') {
            $scope.showTwoPhase = $scope.showTwoPhase === false ? true : false;
        }

        else if (status === 'Single Phase Running') {
            $scope.showSinglePhase = $scope.showSinglePhase === false ? true : false;
        }

        else if (status === 'Power Failure') {
            $scope.showPowerFailurePhase = $scope.showPowerFailurePhase === false ? true : false;
        }

        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        phaseGrid(estatus);
    };

    $scope.halfPekloadClick = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.showHalfPeakLoad = $scope.showHalfPeakLoad === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        halfPeakLoadGrid(pdate);
    };

    $scope.subPeakLoadClick = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.showSubfPeakLoad = $scope.showSubfPeakLoad === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        subPeakLoad(pdate);
    };

    $scope.pfClick = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.showPF = $scope.showPF === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        pFactor(pdate);
    };

    $scope.overLoadClick = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.showOverload = $scope.showOverload === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        overLoad(pdate);
    };

    $scope.intClick = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.showInt = $scope.showHalfPeakLoad === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        int(pdate);
    };

    $scope.instantClick = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.showInstants = $scope.showInstants === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        instants(pdate);
    };

    $scope.logBookClicked = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.showLogBook = $scope.showLogBook === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        logBookGrid(pdate);
    };

    $scope.notCommClick = function (urlDate) {
        $scope.loading = true;
        $scope.noData = true;
        $scope.shownotComm = $scope.shownotComm === false ? true : false;
        urlDate = $filter('date')(urlDate, 'yyyy-MM-dd');
        pdate = urlDate;
        notComm(pdate);
    };

    function phaseGrid(estatus) {

        $scope.phaseGridOptions = {
            enableGridMenu: true,
            exporterMenuCsv: false,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'S.No', field: 'SNo', enableFiltering: false, enableColumnMenu: false },
                { displayName: '33kV SS', field: 'Substation', enableColumnMenu: false },
                { displayName: '11kV Feeder', field: 'FeederName', enableColumnMenu: false },
                { name: 'Type', field: 'Type', enableColumnMenu: false },
                { name: 'Area', field: 'Area', enableColumnMenu: false },
                {
                    name: 'MeterNo', field: 'MeterNo',
                    cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.edit(row.entity)" > {{COL_FIELD}}</a> ',
                    enableColumnMenu: false
                },
                { name: 'MF', field: 'MF', enableFiltering: false, enableColumnMenu: false },
                { name: 'Date', field: 'RTC', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Duration(Hrs:Mins)', field: 'Duration', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'No.of Spells', field: 'Spells', enableFiltering: false, enableColumnMenu: false },
                {
                    name: 'Action',
                    cellTemplate: '<div class="actions">' +
                              '<a ng-click="grid.appScope.GetPhaseHistory(row.entity)"><img src="images/History.gif" /></a>' +
                               '<a ng-click="grid.appScope.GetLiveSubstation(row.entity)" target="_blank"><img src="images/graph.gif" /></a>' +
                              '</div>',
                    enableColumnMenu: false,
                    enableFiltering: false
                }
            ]
        };


        if (estatus == 'Three Phase Running') {
            $scope.threePhasegridOptions = $scope.phaseGridOptions;
        }

        else if (estatus == 'Two Phase Running') {
            $scope.twoPhasegridOptions = $scope.phaseGridOptions;
        }

        else if (estatus == 'Single Phase Running') {
            $scope.singlePhasegridOptions = $scope.phaseGridOptions;
        }

        else if (estatus == 'Power Failure') {
            $scope.PowerFailurePhasegridOptions = $scope.phaseGridOptions;
        }

        $rootScope.data = $scope.phaseGridOptions;

        $scope.edit = function (row) {
            // debugger;

































            var url = 'feederMaster1.html?meterNo=' + row.MeterNo + '&zone=' + zoneval + '&circle=' + circleval + '&mla=' + mlaval + '&div=' + divval + '&subDiv=' + subdivval + '&jen=' + jenval + '&date=' + pdate;
            //var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            //var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            //var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            //var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var x = screen.width / 2 - 1250 / 2;
            var y = screen.height / 2 - 740 / 2;
            var $popup = $window.open(url, 'hai', "height=710,width=1250,status=yes,left=" + x + ",top=" + y);
            $cookies.put('myFavorite', row.Substation);
            $popup.Name = row.Substation;
            $popup.feedername = row.FeederName;
            $popup.type = row.Type;
            $popup.area = row.Area;
            $popup.substation = row.Substation;
            $popup.meterno = row.MeterNo;
            $popup.Test = "test1";
            $popup.userid = userid;
            $popup.pdate = pdate;
        };

        phaseService.phases(zoneval, circleval, mlaval, divval, subdivval, jenval, estatus, pdate).then(function (phasedata) {

            if (estatus == 'Three Phase Running') {
                $scope.threePhasegridOptions.data = phasedata.data;
            }

            else if (estatus == 'Two Phase Running') {
                $scope.twoPhasegridOptions.data = phasedata.data;
            }

            else if (estatus == 'Single Phase Running') {
                $scope.singlePhasegridOptions.data = phasedata.data;
            }

            else if (estatus == 'Power Failure') {
                $scope.PowerFailurePhasegridOptions.data = phasedata.data;
            }

            $rootScope.data.data = phasedata.data;
            $scope.ima = false;
            if (phasedata.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        })

        $scope.GetPhaseHistory = function (field) {
            debugger;
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("PhaseHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;
            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');
            var event = estatus;
            var meterno = field.MeterNo;
            var startdate = historyDate;
            var enddate = historyPrevDate;
            var substation = field.Substation;
            var feeder = field.FeederName;
            $popup.evnt = event;
            $popup.mno = meterno;
            $popup.sdate = startdate;
            $popup.edate = enddate;
            $popup.sstation = substation;
            $popup.fdr = feeder;

            areaService.area(substation.replace('/', '.'), feeder).then(function (areadata) {
                console.log(areadata);
                $popup.adata = areadata.data;

            });
        };

        $scope.GetLiveSubstation = function (field) {
            debugger;
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("LiveSubstation.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var substation = field.Substation;
            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            $popup.nssub = substation;
            $popup.ndate = historyDate;
        };

    };



    function halfPeakLoadGrid(pdate) {

        $scope.halfpeakloadgridOptions = {
            enableGridMenu: true,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'S.No', field: 'SNo', enableFiltering: false, enableColumnMenu: false },
                { displayName: '33kV SS', field: 'SUbStation', enableColumnMenu: false },
                { displayName: '11kV Feeder', field: 'FeederName', enableColumnMenu: false },
                { name: 'Type', field: 'Type' },

                { name: 'Area', field: 'Area', enableFiltering: false, enableColumnMenu: false },
                {
                    name: 'MeterNo', field: 'MeterNo',
                    cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.edit(row.entity)" > {{COL_FIELD}}</a> ',
                    enableColumnMenu: false
                },
                { name: 'MF', field: 'MF', enableFiltering: false, enableColumnMenu: false },
                { name: 'Date', field: 'RTC', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'IR', field: 'Ir', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'IY', field: 'Iy', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'IB', field: 'Ib', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Avg.Current(Amps)', field: 'AvgCur', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'kVA', field: 'kVA', enableFiltering: false, enableColumnMenu: false },
                {
                    name: 'Action',
                    cellTemplate: '<div class="actions">' +
                              '<a ng-click="grid.appScope.GetHalfPeakloadHistory(row.entity)"><img src="images/History.gif" /></a>' +
                               '<a ng-click="grid.appScope.GetHalfPeakloadGraph(row.entity)"><img src="images/graph.gif" /></a>' +
                              '</div>',
                    enableColumnMenu: false,
                    enableFiltering: false
                }
            ]
        };

        $scope.edit = function (row) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;




            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("test.htm", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);








            $cookies.put('myFavorite', row.Substation);
            $popup.Name = row.Substation;
            $popup.Test = "test1";

        };

        halfPeakLoadService.halfPeakLoad(zoneval, circleval, mlaval, divval, subdivval, jenval, pdate).then(function (data) {
            $scope.halfpeakloadgridOptions.data = data.data;
            if (data.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });

        $scope.GetHalfPeakloadHistory = function (field) {
            debugger;
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("HalfPeakloadHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');

            var meterno = field.MeterNo;
            var startdate = historyDate;
            var enddate = historyPrevDate;
            var substation = field.SUbStation;
            var feeder = field.FeederName;

            $popup.mno = meterno;
            $popup.sdate = startdate;
            $popup.edate = enddate;
            $popup.sstation = substation;
            $popup.fdr = feeder;

            areaService.area(substation.replace('/', '.'), feeder).then(function (areadata) {
                console.log(areadata);
                $popup.adata = areadata.data;

            });
        };
        $scope.GetHalfPeakloadGraph = function (field) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("HalfPeakloadGraph.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var substation = field.SUbStation;
            var feeder = field.FeederName;
            var type = field.Type;
            var area = field.Area;
            var meterno = field.MeterNo;

            $popup.subs = substation;
            $popup.fdr = feeder;
            $popup.stype = type;
            $popup.sarea = area;
            $popup.mno = meterno;



        };

    };

    function subPeakLoad(pdate) {
        $scope.subpeakloadgridOptions = {
            enableGridMenu: true,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'S.No', field: 'SNo', enableFiltering: false, enableColumnMenu: false },
                { displayName: '33kV SS', field: 'substation', enableColumnMenu: false },
                { displayName: 'DateTime', field: 'meterdate', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'IR', field: 'Ir', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'IY', field: 'Iy', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'IB', field: 'Ib', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Avg.Current (Amps)', field: 'AvgCur', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'kVA', field: 'kVa', enableFiltering: false, enableColumnMenu: false },
                {
                    name: 'Action',
                    cellTemplate: '<div class="actions">' +
                              '<a ng-click="grid.appScope.GetSubstationPeakloadHistory(row.entity)"><img src="images/History.gif" /></a>' +
                               '<a ng-click="grid.appScope.GetSubstationPeakloadGraph(row.entity)"><img src="images/graph.gif" /></a>' +
                              '</div>',
                    enableColumnMenu: false,
                    enableFiltering: false
                }

            ]
        };

        substationPeakLoadService.subPeak(zoneval, circleval, mlaval, divval, subdivval, jenval, pdate).then(function (data) {
            $scope.subpeakloadgridOptions.data = data.data;
            if (data.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });

        $scope.GetSubstationPeakloadHistory = function (field) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("SubstationPeakloadHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');


            var startdate = historyDate;
            var enddate = historyPrevDate;
            var substation = field.substation;
            var feeder = field.FeederName;
            $popup.zone = zoneval;
            $popup.circle = circleval;
            $popup.mla = mlaval;
            $popup.div = divval;
            $popup.subdiv = subdivval;
            $popup.jen = jenval;

            $popup.sdate = startdate;






            $popup.edate = enddate;
            $popup.sstation = substation;
            $popup.fdr = feeder;


            areaService.area(substation.replace('/', '.'), feeder).then(function (areadata) {
                console.log(areadata);
                $popup.adata = areadata.data;




            });
        };
        $scope.GetSubstationPeakloadGraph = function (field) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("SubstationPeakloadGraph.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var substation = field.substation;

            $popup.subs = substation;

        };
    };

    function pFactor(pdate) {
        $scope.pfGridOptions = {
            enableGridMenu: true,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'S.No', field: 'SNo', enableFiltering: false, enableColumnMenu: false },
                { displayName: '33kV SS', field: 'SubStation', enableColumnMenu: false },
                { displayName: '11kV Feeder', field: 'FeederName', enableColumnMenu: false },
                { name: 'Type', field: 'Type', enableFiltering: false, enableColumnMenu: false },
                { name: 'Area', field: 'Area', enableFiltering: false, enableColumnMenu: false },
                {
                    field: 'Meterno', name: 'Meterno',
                    cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.edit(row.entity)" > {{COL_FIELD}}</a> ',
                    enableColumnMenu: false
                },
                {
                    field: 'AbovePF', displayName: 'Above 0.9',

                    cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetPFAbstractHistory(row.entity,1)" > {{COL_FIELD}}</a> ',
                    enableFiltering: false,
                    enableColumnMenu: false
                },
                 {
                     field: 'BelowPf', displayName: 'Below 0.9',

                     cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetPFAbstractHistory(row.entity,2)" > {{COL_FIELD}}</a> ',
                     enableFiltering: false,
                     enableColumnMenu: false
                 },
                 {
                     name: 'Action',
                     cellTemplate: '<div class="actions">' +
                               '<a ng-click="grid.appScope.GetPFAbstractHistory(row.entity,0)"><img src="images/History.gif" /></a>' +
                                '<a ng-click="grid.appScope.GetPFAbstractGraph(row.entity)"><img src="images/graph.gif" /></a>' +
                               '</div>',
                     enableColumnMenu: false,
                     enableFiltering: false
                 }
            ]
        };

        $scope.edit = function (row) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("test.htm", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);
            $cookies.put('myFavorite', row.Substation);
            $popup.Name = row.Substation;
            $popup.Test = "test1";

        };

        pfAbstractService.pfAbstract(zoneval, circleval, mlaval, divval, subdivval, jenval, pdate).then(function (data) {
            console.log(data.data);
            $scope.pfGridOptions.data = data.data;
            if (data.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });

        $scope.GetPFAbstractHistory = function (field, status) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("PFAbstractHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');







            var spf = status;

            if (spf === 1 || spf === 2) {
                $popup.hidedate = true;


                var startdate = historyDate;
                var enddate = historyDate
            }
            else if (spf === 0) {
                var startdate = historyDate;
                var enddate = historyPrevDate
            }
            var meterno = field.Meterno;
            var substation = field.SubStation;
            var feeder = field.FeederName;

            $popup.mno = meterno;
            $popup.sdate = startdate;
            $popup.edate = enddate;
            $popup.sstation = substation;
            $popup.fdr = feeder;
            $popup.pf = spf;

            areaService.area(substation.replace('/', '.'), feeder).then(function (areadata) {
                console.log(areadata);
                $popup.adata = areadata.data;





            });
        };

        $scope.GetPFAbstractGraph = function (field) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("PowerFactorGraph.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var substation = field.SubStation;
            var feeder = field.FeederName;
            var type = field.Type;
            var area = field.Area;
            var meterno = field.Meterno;

            $popup.subs = substation;
            $popup.fdr = feeder;
            $popup.stype = type;
            $popup.sarea = area;
            $popup.mno = meterno;



        };
    };

    function overLoad(pdate) {
        $scope.overLoadGridOptions = {
            enableGridMenu: true,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'S.No', field: 'SNo', enableFiltering: false, enableColumnMenu: false },
                { displayName: '33kV SS', field: 'SubStation', enableColumnMenu: false },
                 {
                     field: 'Overload', displayName: 'Above 100A',

                     cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetOverloadParent(row.entity,1)" > {{COL_FIELD}}</a> ',
                     enableFiltering: false,
                     enableColumnMenu: false
                 },
                  {
                      field: 'Underload', displayName: 'Below 100A',

                      cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetOverloadParent(row.entity,2)" > {{COL_FIELD}}</a> ',
                      enableFiltering: false,
                      enableColumnMenu: false
                  },
                {
                    name: 'Action',
                    cellTemplate: '<div class="actions">' +
                              '<a ng-click="grid.appScope.GetOverloadHistory(row.entity)"><img src="images/History.gif" /></a>' +
                               '<a ng-click="grid.appScope.GetOverloadGraph(row.entity)"><img src="images/graph.gif" /></a>' +
                              '</div>',
                    enableFiltering: false,
                    enableColumnMenu: false
                }
            ]
        };

        overloadService.overload(zoneval, circleval, mlaval, divval, subdivval, jenval, pdate).then(function (data) {
            $scope.overLoadGridOptions.data = data.data;
            if (data.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });

        $scope.GetOverloadHistory = function (field) {

            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("OverloadHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');


            var startdate = historyDate;
            var enddate = historyPrevDate;
            var substation = field.SubStation;
            var feeder = field.FeederName;
            $popup.zone = zoneval;
            $popup.circle = circleval;
            $popup.mla = mlaval;
            $popup.div = divval;
            $popup.subdiv = subdivval;
            $popup.jen = jenval;

            $popup.sdate = startdate;
            $popup.edate = enddate;
        };
        $scope.GetOverloadParent = function (field, status) {
            debugger;
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;


            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;





            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("OverloadParent.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');


            var startdate = historyDate;
            var enddate = historyDate;
            var substation = field.SubStation;
            var feeder = field.FeederName;
            $popup.zone = zoneval;
            $popup.circle = circleval;
            $popup.mla = mlaval;
            $popup.div = divval;
            $popup.subdiv = subdivval;
            $popup.jen = jenval;
            $popup.sload = status;
            $popup.ssubt = substation;
            $popup.sdate = startdate;
            $popup.edate = enddate;
        };
        $scope.GetOverloadGraph = function (field) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("OverloadGraph.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var substation = field.SubStation;

            $popup.subs = substation;




        };
    };

    function int(pdate) {
        $scope.intGridOptions = {
            enableGridMenu: true,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
               { name: 'S.No', field: 'SNo', enableFiltering: false, enableColumnMenu: false },
                { displayName: '33kV SS', field: 'SubStation', enableColumnMenu: false },
                 {
                     field: 'PF1', displayName: '<=3 Interruption',

                     cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetInterruptionParent(row.entity,1)" > {{COL_FIELD}}</a> ',
                     enableFiltering: false,
                     enableColumnMenu: false
                 },
                  {
                      field: 'PF2', displayName: '>3 and <=6 Interruption',

                      cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetInterruptionParent(row.entity,2)" > {{COL_FIELD}}</a> ',
                      enableFiltering: false,
                      enableColumnMenu: false
                  },
                   {
                       field: 'PF3', displayName: '>6 and <=10 Interruption',

                       cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetInterruptionParent(row.entity,3)" > {{COL_FIELD}}</a> ',
                       enableFiltering: false,
                       enableColumnMenu: false
                   },
                    {
                        field: 'PF4', displayName: '>10 Interruption',

                        cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.GetInterruptionParent(row.entity,4)" > {{COL_FIELD}}</a> ',
                        enableFiltering: false,
                        enableColumnMenu: false
                    },
                {
                    name: 'Action',
                    cellTemplate: '<div class="actions">' +
                              '<a ng-click="grid.appScope.GetInterruptionHistory(row.entity)"><img src="images/History.gif" /></a>' +
                               '<a ng-click="grid.appScope.GetLiveSubstation(row.entity)"><img src="images/graph.gif" /></a>' +
                              '</div>',
                    enableFiltering: false,
                    enableColumnMenu: false
                }
            ]
        };

        interruptionService.interruption(zoneval, circleval, mlaval, divval, subdivval, jenval, pdate).then(function (data) {
            $scope.intGridOptions.data = data.data;
            if (data.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });

        $scope.GetInterruptionHistory = function (field) {

            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("InterruptionHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');


            var startdate = historyDate;
            var enddate = historyPrevDate;
            var substation = field.SubStation;
            $popup.zone = zoneval;
            $popup.circle = circleval;
            $popup.mla = mlaval;
            $popup.div = divval;
            $popup.subdiv = subdivval;
            $popup.jen = jenval;

            $popup.sdate = startdate;
            $popup.edate = enddate;
        };
        $scope.GetInterruptionParent = function (field, status) {
            debugger;
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;





            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;


            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("InterruptionParent.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');


            var startdate = historyDate;
            var enddate = historyDate;
            var substation = field.SubStation;
            var feeder = field.FeederName;
            $popup.zone = zoneval;
            $popup.circle = circleval;
            $popup.mla = mlaval;
            $popup.div = divval;
            $popup.subdiv = subdivval;
            $popup.jen = jenval;
            $popup.spfspell = status;
            $popup.ssubt = substation;
            $popup.sdate = startdate;
            $popup.edate = enddate;
        };
        $scope.GetLiveSubstation = function (field) {
            debugger;
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("LiveSubstation.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var substation = field.SubStation;
            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            $popup.nssub = substation;
            $popup.ndate = historyDate;
        };
    };

    function instants(pdate) {
        $scope.instantsGridOptions = {
            enableGridMenu: true,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'S.No', field: 'SNo', enableFiltering: false, enableColumnMenu: false },
                { displayName: '33kV SS', field: 'SubStation', enableColumnMenu: false },
                { displayName: '11kV Feeder', field: 'FeederName', enableColumnMenu: false },
                { name: 'Type', field: 'Type', enableColumnMenu: false },
                { name: 'Area', field: 'Area', enableFiltering: false, enableColumnMenu: false },
                {
                    field: 'MeterNo', name: 'MeterNo',
                    cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.edit(row.entity)" > {{COL_FIELD}}</a> ',
                    enableColumnMenu: false
                },
                { name: 'MF', field: 'MF', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'DateTime', field: 'RTC', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Cum.kWh', field: 'ActEng_CumFWD', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Cum.kVArh(lg)', field: 'ReaEng_CumFWD', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Cum.kVArh(ld)', field: 'ReaEng_CumREV', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Cum.kVAh(ld)', field: 'AppEng_CumFWD', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Avg.Pf', field: 'avgPF_FWD', enableFiltering: false, enableColumnMenu: false },
                { displayName: 'Frequency', field: 'Freq', enableFiltering: false, enableColumnMenu: false },
                {
                    name: 'Action',
                    cellTemplate: '<div class="actions">' +
                              '<a ng-click="grid.appScope.GetInstantHistory(row.entity)"><img src="images/History.gif" /></a>' +
                              '</div>',
                    enableFiltering: false,
                    enableColumnMenu: false
                }
            ]
        };











        $scope.edit = function (row) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;


            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("test.htm", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);
            $cookies.put('myFavorite', row.Substation);
            $popup.Name = row.Substation;
            $popup.Test = "test1";

        };

        instantsService.instants(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (data) {
            $scope.instantsGridOptions.data = data.data;
            if (data.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });

        $scope.GetInstantHistory = function (field) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("MeterInstantsHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');


            var startdate = historyDate;
            var enddate = historyPrevDate;
            var meterno = field.MeterNo;
            $popup.zone = zoneval;
            $popup.circle = circleval;
            $popup.mla = mlaval;
            $popup.div = divval;
            $popup.subdiv = subdivval;
            $popup.jen = jenval;
            $popup.smeterno = meterno;

            $popup.sdate = startdate;
            $popup.edate = enddate;
        };
    };

    function logBookGrid() {
        $scope.logBookGridOptions = {
            expandableRowTemplate: 'expandableRowTemplate.html',
            expandableRowHeight: 150,
            onRegisterApi: function (gridApi) {
                gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                    console.log(row.entity.Meterno);
                    if (row.isExpanded) {
                        row.entity.subGridOptions = {
                            columnDefs: [
                            { displayName: 'S.No', name: 'SNo', enableColumnMenu: false },

                            { displayName: 'DateTime', name: 'DateTime', width: '150', enableColumnMenu: false },
                            { displayName: 'MF', name: 'MF', enableColumnMenu: false },
                            { name: 'Reading', enableColumnMenu: false },
                            { displayName: 'Current', name: 'load_cur', enableColumnMenu: false },
                            { displayName: 'kVA', name: 'load_kva', enableColumnMenu: false },
                            { displayName: 'kWh Cons.', name: 'kwh_Cons', enableColumnMenu: false },
                             { displayName: 'kVArg(lg)', name: 'kvarh_lag', enableColumnMenu: false },
                            { displayName: 'kVArh(ld)', name: 'kvarh_lead', enableColumnMenu: false },
                            { displayName: 'IR', name: 'Ir', enableColumnMenu: false },
                            { displayName: 'IY', name: 'Iy', enableColumnMenu: false },
                            { displayName: 'IB', name: 'Ib', enableColumnMenu: false },
                            { displayName: 'VR', name: 'Vr', enableColumnMenu: false },
                            { displayName: 'VY', name: 'Vy', enableColumnMenu: false },

                            { displayName: 'VB', name: 'Vb', enableColumnMenu: false },

                            { displayName: 'PF', name: 'PF', enableColumnMenu: false },
                            { displayName: 'EventDesc', name: 'EventDesc', enableColumnMenu: false },
                            ]
                        };
                        $cookies.put("meterno", row.entity.Meterno);
                        logChild.logChild(zoneval, circleval, mlaval, divval, subdivval, jenval, pdate, row.entity.Meterno).then(function (childdata) {
                            row.entity.subGridOptions.data = childdata.data;
                        });

                    }
                });
            },

            columnDefs: [
          { displayName: '33kV SS', name: 'Substation', pinnedLeft: true },
          { displayName: '11kV Feeder', name: 'FeederName' },
          {
              displayName: 'MeterNo',
              name: 'Meterno',
              field: 'Meterno',
              cellTemplate: '<a id="editBtn" class="btn-small" href="https://stackoverflow.com/questions/25848416/angularjs-ui-grid-render-hyperlink" > {{COL_FIELD}}</a> '

          },
          { name: 'Type' },
          {
              name: 'Action',
              cellTemplate: '<div class="actions">' +
                        '<a ng-click="grid.appScope.GetLogBookHistory(row.entity)"><img src="images/History.gif" /></a>' +
                        '</div>'


          }
            ]

        };

        logParent.logParent(zoneval, circleval, mlaval, divval, subdivval, jenval, pdate).then(function (parentdata) {
            debugger;
            $scope.logBookGridOptions.data = parentdata.data;
            if (parentdata.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        });

        $scope.GetLogBookHistory = function (field) {
            debugger;
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("LogBookHistory.html", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);

            var yes = new Date(pdate);
            var mm = yes.getMonth();
            var yy = yes.getFullYear();
            var dd = yes.getDate() - 1;

            var historyDate = $filter('date')(pdate, 'dd-MM-yyyy');
            var historyPrevDate = $filter('date')(new Date(yy, mm, dd), 'dd-MM-yyyy');









            var startdate = historyDate;
            var enddate = historyPrevDate;
            var meterno = field.Meterno;
            $popup.zone = zoneval;
            $popup.circle = circleval;
            $popup.mla = mlaval;
            $popup.div = divval;
            $popup.subdiv = subdivval;
            $popup.jen = jenval;
            $popup.smeterno = meterno;

            $popup.sdate = startdate;
            $popup.edate = enddate;
        };





    };

    function notComm(pdate) {
        $scope.notCommGridOptions = {
            enableGridMenu: true,
            exporterCsvFilename: 'myFile.csv',
            exporterPdfDefaultStyle: { fontSize: 9 },
            exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
            exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
            exporterPdfFooter: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function (docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfOrientation: 'portrait',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 500,
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            exporterExcelFilename: 'myFile.xlsx',
            exporterExcelSheetName: 'Sheet1',
            paginationPageSizes: [10, 20, 30],
            paginationPageSize: 10,
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { displayName: 'S.No', field: 'SNo', enableColumnMenu: false, enableFiltering: false },
                { field: 'Zone', enableColumnMenu: false },
                { field: 'Circle', enableColumnMenu: false },
                { field: 'MLA', enableColumnMenu: false },
                { field: 'Division', enableColumnMenu: false },
                {
                    field: 'Meterno', name: 'Meterno', displayName: 'MeterNo',
                    cellTemplate: '<a id="editBtn" class="btn-small" ng-click="grid.appScope.edit(row.entity)" > {{COL_FIELD}}</a> ',
                    enableColumnMenu: false
                },
                { field: 'SubDivision', enableColumnMenu: false },
                { displayName: 'Junior Engineer', field: 'Section', enableColumnMenu: false },
                { displayName: '33kV SS', field: 'SubStation', enableColumnMenu: false },
                { displayName: '11kV Feeder', field: 'FeederName', enableColumnMenu: false },
                { field: 'Type', enableColumnMenu: false },
                { field: 'Area', enableColumnMenu: false, enableFiltering: false },
                { displayName: 'MF', field: 'MF', enableColumnMenu: false, enableFiltering: false },
                { displayName: 'Reg.DateTime', field: 'RegDate', enableColumnMenu: false, enableFiltering: false },
                { displayName: 'Schedule', field: 'Schdule', enableColumnMenu: false, enableFiltering: false },
                { displayName: 'Comm.DateTime', field: 'CommDate', enableColumnMenu: false, enableFiltering: false },
                { field: 'EventStatus', enableColumnMenu: false, enableFiltering: false },
                { field: 'Remarks', enableColumnMenu: false, enableFiltering: false }
            ]
        };








        $scope.edit = function (row) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;


            var left = ((width / 2) - (screen.width / 2)) + dualScreenLeft;
            var top = ((height / 2) - (screen.height / 2)) + dualScreenTop;
            var $popup = $window.open("test.htm", 'PopUp', 'scrollbars=yes, menubar=no,resizable=no, width=' + screen.width + ', height=' + screen.height + ', top=' + top + ', left=' + left);
            $cookies.put('myFavorite', row.Substation);
            $popup.Name = row.Substation;
            $popup.Test = "test1";

        };

        notCommService.notComm(zoneval, circleval, mlaval, divval, subdivval, jenval).then(function (data) {
            console.log(data);
            $scope.notCommGridOptions.data = data.data;
            if (data.data.length === 0) {
                $scope.noData = false;
            }
            $scope.loading = false;
        })
    }

})

.controller('testController', function ($scope, $routeParams, $window) {
    $scope.testid = $window.substation;
    alert($scope.testid);


});

